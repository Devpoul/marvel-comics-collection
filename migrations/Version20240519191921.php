<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240519191921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE portrait_character (portrait_id INT NOT NULL, character_id INT NOT NULL, PRIMARY KEY(portrait_id, character_id))');
        $this->addSql('CREATE INDEX IDX_D1FA6CEF1226EBF3 ON portrait_character (portrait_id)');
        $this->addSql('CREATE INDEX IDX_D1FA6CEF1136BE75 ON portrait_character (character_id)');
        $this->addSql('ALTER TABLE portrait_character ADD CONSTRAINT FK_D1FA6CEF1226EBF3 FOREIGN KEY (portrait_id) REFERENCES portrait (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE portrait_character ADD CONSTRAINT FK_D1FA6CEF1136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character DROP portrait');
        $this->addSql('ALTER TABLE portrait DROP CONSTRAINT fk_954034fb16a2b381');
        $this->addSql('DROP INDEX idx_954034fb16a2b381');
        $this->addSql('ALTER TABLE portrait DROP book_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE portrait_character DROP CONSTRAINT FK_D1FA6CEF1226EBF3');
        $this->addSql('ALTER TABLE portrait_character DROP CONSTRAINT FK_D1FA6CEF1136BE75');
        $this->addSql('DROP TABLE portrait_character');
        $this->addSql('ALTER TABLE character ADD portrait VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE portrait ADD book_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE portrait ADD CONSTRAINT fk_954034fb16a2b381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_954034fb16a2b381 ON portrait (book_id)');
    }
}
