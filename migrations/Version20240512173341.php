<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240512173341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE portrait_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE portrait (id INT NOT NULL, book_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, image_size INT DEFAULT NULL, mime_type VARCHAR(255) DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_954034FB16A2B381 ON portrait (book_id)');
        $this->addSql('COMMENT ON COLUMN portrait.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE portrait ADD CONSTRAINT FK_954034FB16A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cover RENAME COLUMN cover_name TO name');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE portrait_id_seq CASCADE');
        $this->addSql('ALTER TABLE portrait DROP CONSTRAINT FK_954034FB16A2B381');
        $this->addSql('DROP TABLE portrait');
        $this->addSql('ALTER TABLE cover RENAME COLUMN name TO cover_name');
    }
}
