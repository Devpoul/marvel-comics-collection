<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240512004012 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE event_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE nature_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE team_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE event (id INT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE nature (id INT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE team (id INT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE team_character (team_id INT NOT NULL, character_id INT NOT NULL, PRIMARY KEY(team_id, character_id))');
        $this->addSql('CREATE INDEX IDX_247FAED3296CD8AE ON team_character (team_id)');
        $this->addSql('CREATE INDEX IDX_247FAED31136BE75 ON team_character (character_id)');
        $this->addSql('ALTER TABLE team_character ADD CONSTRAINT FK_247FAED3296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE team_character ADD CONSTRAINT FK_247FAED31136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book ADD event_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A33171F7E88B FOREIGN KEY (event_id) REFERENCES event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CBE5A33171F7E88B ON book (event_id)');
        $this->addSql('ALTER TABLE character ADD nature_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT FK_937AB0343BCB2E4B FOREIGN KEY (nature_id) REFERENCES nature (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_937AB0343BCB2E4B ON character (nature_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE book DROP CONSTRAINT FK_CBE5A33171F7E88B');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB0343BCB2E4B');
        $this->addSql('DROP SEQUENCE event_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE nature_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE team_id_seq CASCADE');
        $this->addSql('ALTER TABLE team_character DROP CONSTRAINT FK_247FAED3296CD8AE');
        $this->addSql('ALTER TABLE team_character DROP CONSTRAINT FK_247FAED31136BE75');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE nature');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_character');
        $this->addSql('DROP INDEX IDX_937AB0343BCB2E4B');
        $this->addSql('ALTER TABLE character DROP nature_id');
        $this->addSql('DROP INDEX IDX_CBE5A33171F7E88B');
        $this->addSql('ALTER TABLE book DROP event_id');
    }
}
