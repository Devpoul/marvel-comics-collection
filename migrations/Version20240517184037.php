<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240517184037 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cover_book (cover_id INT NOT NULL, book_id INT NOT NULL, PRIMARY KEY(cover_id, book_id))');
        $this->addSql('CREATE INDEX IDX_FF39EAD8922726E9 ON cover_book (cover_id)');
        $this->addSql('CREATE INDEX IDX_FF39EAD816A2B381 ON cover_book (book_id)');
        $this->addSql('ALTER TABLE cover_book ADD CONSTRAINT FK_FF39EAD8922726E9 FOREIGN KEY (cover_id) REFERENCES cover (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cover_book ADD CONSTRAINT FK_FF39EAD816A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cover DROP CONSTRAINT fk_8d0886c516a2b381');
        $this->addSql('DROP INDEX idx_8d0886c516a2b381');
        $this->addSql('ALTER TABLE cover DROP book_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE cover_book DROP CONSTRAINT FK_FF39EAD8922726E9');
        $this->addSql('ALTER TABLE cover_book DROP CONSTRAINT FK_FF39EAD816A2B381');
        $this->addSql('DROP TABLE cover_book');
        $this->addSql('ALTER TABLE cover ADD book_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cover ADD CONSTRAINT fk_8d0886c516a2b381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8d0886c516a2b381 ON cover (book_id)');
    }
}
