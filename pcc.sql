--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3
-- Dumped by pg_dump version 16.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
            BEGIN
                PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author (
    id integer NOT NULL,
    first_name character varying(255) DEFAULT NULL::character varying,
    last_name character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) DEFAULT NULL::character varying,
    date_of_birth timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    date_of_death timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.author OWNER TO postgres;

--
-- Name: COLUMN author.date_of_birth; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.author.date_of_birth IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN author.date_of_death; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.author.date_of_death IS '(DC2Type:datetime_immutable)';


--
-- Name: author_book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author_book (
    author_id integer NOT NULL,
    book_id integer NOT NULL
);


ALTER TABLE public.author_book OWNER TO postgres;

--
-- Name: author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.author_id_seq OWNER TO postgres;

--
-- Name: book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    sub_title character varying(255) DEFAULT NULL::character varying,
    volume integer,
    summary text,
    pages integer,
    release timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    event_id integer,
    univers_id integer
);


ALTER TABLE public.book OWNER TO postgres;

--
-- Name: COLUMN book.release; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.book.release IS '(DC2Type:datetime_immutable)';


--
-- Name: book_character; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book_character (
    book_id integer NOT NULL,
    character_id integer NOT NULL
);


ALTER TABLE public.book_character OWNER TO postgres;

--
-- Name: book_format; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book_format (
    book_id integer NOT NULL,
    format_id integer NOT NULL
);


ALTER TABLE public.book_format OWNER TO postgres;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.book_id_seq OWNER TO postgres;

--
-- Name: character; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."character" (
    id integer NOT NULL,
    first_name character varying(255) DEFAULT NULL::character varying,
    last_name character varying(255) DEFAULT NULL::character varying,
    alias character varying(255) NOT NULL,
    side boolean,
    nature_id integer
);


ALTER TABLE public."character" OWNER TO postgres;

--
-- Name: character_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.character_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.character_id_seq OWNER TO postgres;

--
-- Name: cover; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cover (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    image_size integer,
    mime_type character varying(255) DEFAULT NULL::character varying,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.cover OWNER TO postgres;

--
-- Name: COLUMN cover.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.cover.updated_at IS '(DC2Type:datetime_immutable)';


--
-- Name: cover_book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cover_book (
    cover_id integer NOT NULL,
    book_id integer NOT NULL
);


ALTER TABLE public.cover_book OWNER TO postgres;

--
-- Name: cover_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cover_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.cover_id_seq OWNER TO postgres;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO postgres;

--
-- Name: event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event (
    id integer NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE public.event OWNER TO postgres;

--
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.event_id_seq OWNER TO postgres;

--
-- Name: format; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.format (
    id integer NOT NULL,
    entitled character varying(255) NOT NULL,
    description text
);


ALTER TABLE public.format OWNER TO postgres;

--
-- Name: format_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.format_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.format_id_seq OWNER TO postgres;

--
-- Name: job; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.job (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text
);


ALTER TABLE public.job OWNER TO postgres;

--
-- Name: job_author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.job_author (
    job_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.job_author OWNER TO postgres;

--
-- Name: job_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.job_id_seq OWNER TO postgres;

--
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO postgres;

--
-- Name: COLUMN messenger_messages.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.available_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.available_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.delivered_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.delivered_at IS '(DC2Type:datetime_immutable)';


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.messenger_messages_id_seq OWNER TO postgres;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- Name: nature; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nature (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text
);


ALTER TABLE public.nature OWNER TO postgres;

--
-- Name: nature_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nature_id_seq OWNER TO postgres;

--
-- Name: portrait; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.portrait (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    image_size integer,
    mime_type character varying(255) DEFAULT NULL::character varying,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.portrait OWNER TO postgres;

--
-- Name: COLUMN portrait.updated_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.portrait.updated_at IS '(DC2Type:datetime_immutable)';


--
-- Name: portrait_character; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.portrait_character (
    portrait_id integer NOT NULL,
    character_id integer NOT NULL
);


ALTER TABLE public.portrait_character OWNER TO postgres;

--
-- Name: portrait_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.portrait_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.portrait_id_seq OWNER TO postgres;

--
-- Name: team; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text
);


ALTER TABLE public.team OWNER TO postgres;

--
-- Name: team_character; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team_character (
    team_id integer NOT NULL,
    character_id integer NOT NULL
);


ALTER TABLE public.team_character OWNER TO postgres;

--
-- Name: team_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.team_id_seq OWNER TO postgres;

--
-- Name: univers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.univers (
    id integer NOT NULL,
    name character varying(255),
    description text
);


ALTER TABLE public.univers OWNER TO postgres;

--
-- Name: univers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.univers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.univers_id_seq OWNER TO postgres;

--
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.author (id, first_name, last_name, alias, date_of_birth, date_of_death) FROM stdin;
\.


--
-- Data for Name: author_book; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.author_book (author_id, book_id) FROM stdin;
\.


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.book (id, title, sub_title, volume, summary, pages, release, event_id, univers_id) FROM stdin;
163	X-O MANOWAR	DE L'AN 402 À AUJOURDH'HUI : L'ÉPOPÉE D'ARIC DE DACIE	1	Aric est un wisigoth, ce peuple arraché à sa terre et contraint de lutter pour sa liberté contre l’empire romain. Une nuit, pensant attaquer des légionnaires, Aric est enlevé par des extraterrestres, les Vignes, et réduit en esclavage. Après des années dans l’espace, il finit par briser ses chaînes et voler l’artéfact le plus sacré des Vignes : l’armure Shanhara. Grâce à elle, il gagne des pouvoirs incroyables et peut rentrer se venger de l’empire romain. Mais il découvre qu’au profit des lois de la physique, 1600 ans se sont écoulées sur Terre. Aric se retrouve de nos jours, privé de sa terre et de son peuple, dans une armure le rendant quasi-invulnérable... L’adaptation risque d’être difficile !	752	2017-12-09 00:00:00	\N	3
174	VIE ET MORT DE TOYO HARADA	LE TESTAMENT DE L’HOMME LE PLUS PUISSANT DU MONDE.	\N	Après avoir survécu à Hiroshima, la bombe même qui, en explosant, a activé ses incroyables pouvoirs psychiques, Toyo Harada s’est juré d’empêcher l’humanité de entre-tuer à nouveau. Toute sa vie serait dédiée à la réalisation de son utopie… quel qu’en soit le prix. Se servant de ses pouvoirs pour devenir l’un des hommes les plus riches et influents du monde, il créé en secret la Fondation Harbinger, où il recrute des psiotiques, des êtres comme lui dotés de pouvoirs, pour agrandir les rangs de son armée privée. Mais depuis, son empire s’est effondré et l’existence des psiotiques et de la Fondation ont été révélés au monde. Poussé à réaliser son plan au grand jour, il conquiert une partie de la Somalie, y installe sa nouvelle Fondation et offre aux pays du tiers-monde qui acceptent de le soutenir, des technologies encore jamais vues. L’équilibre géopolitique du monde est bouleversé, et un conflit ouvert éclate avec les grandes puissances de la planète. Harada sortira-t-il de cette guerre en héros de l’humanité… ou en son plus terrible ennemi ?	224	2019-11-22 00:00:00	\N	3
164	X-O MANOWAR	ARMOR HUNTERS : L’ÉVÉNEMENT QUI A BOULEVERSÉ L’UNIVERS VALIANT !	2	Lorsqu’Aric de Dacie est rentré sur Terre, avec l’armure X-O Manowar volée aux extraterrestres Vignes,  il pensait détenir l’arme parfaite. Il pourrait enfin trouver un territoire pour son peuple wisigoth et protéger ce  nouveau et balbutiant royaume. Mais une force venue des confins de l’espace approche de la Terre : les Chasseurs d’Armures. Ils n’ont qu’un seul but : débarrasser l’univers des armures comme celle d’Aric et leur pouvoir de destruction incommensurable. Qu’importe s’il faut détruire la Terre avec...	592	2017-09-28 00:00:00	\N	3
175	DIVINITY	LA TERRE S’APPRÊTE À ACCUEILLIR UN NOUVEAU DIEU	\N	À l’apogée de la Guerre Froide, l’Union Soviétique est déterminée à gagner la course à l’espace. Elle se lance dans une mission incroyablement ambitieuse : envoyer trois cosmonautes aux confins de l’univers. Des orphelins triés sur le volet, entraînés tels des communistes modèles et interdits de fonder une famille. Au bout de leur voyage interstellaire, ils rencontrent l’Inconnu… quelque chose qui les changera à jamais. Abram est le premier à rentrer sur Terre, s’écrasant de nos jours dans le désert australien. Ceux qui l’ont rencontré le considèrent comme un dieu, capable de plier la matière, l’espace et le temps à sa volonté. Mais malgré ses pouvoirs, il ne peut oublier l’amour de sa vie et l’enfant qu’elle portait, qu’il avait dissimulés à ses supérieurs et abandonnés pour partir à la conquête de l’espace. C’est alors que Myshka, la copilote du groupe, retrouve également le chemin de la Terre. Mais, contrairement à Abram, elle n’a aucune famille secrète et croit toujours en l’idéal communiste. Sa mission : restaurer la gloire soviétique.	496	2019-11-29 00:00:00	\N	3
165	X-O MANOWAR	LE ROI EST MORT, VIVE LE ROI !	3	Devenu protecteur de l’humanité, Aric pensait avoir trouvé la paix. Mais le spectre des Chasseurs d’Armures n’est pas loin, et déclenche une réaction en chaîne aux conséquences terribles pour le peuple Vigne et la planète Terre. Aric sera-t-il à la hauteur de ces défis, avant la venue de l’ultime menace... Les mystérieux Supplices ?	808	2018-02-08 00:00:00	\N	3
176	UNITY	UNITY	\N	L’homme le plus dangereux du monde, Toyo Harada, est saisi d’un sentiment qu’il avait depuis longtemps oublié : la peur. Pour prévenir une catastrophe nucléaire, le plus puissant des psiotiques assemble une équipe composée d’êtres aux capacités extraordinaires : le Guerrier Éternel, la techno-télépathe Livewire et l’agent secret britannique Ninjak. Ensemble, ils forment UNITY. Leur mission : tuer Aric de Dacie, alias X-O Manowar ! Mais les intentions d’Harada sont-elles vraiment aussi claires qu’elles en ont l’air ?	768	2018-11-16 00:00:00	\N	3
166	THE VALIANT	“JE VAIS AVOIR BESOIN D’AIDE. DE BEAUCOUP D’AIDE.”	\N	Gilad est le Guerrier Éternel. Depuis des millénaires, il protège la Terre et ses messagers, les Géomanciens. Par le passé, plusieurs furent tués par l’Ennemi Immortel, un être dont le seul but est d’abattre les ténèbres sur la planète. De nos jours, il revient pour s’attaquer à Kay, la nouvelle Géomancienne qui peine à s’adapter à son nouveau rôle. Mais Gilad peut désormais compter sur les héros de la Terre, et sur Bloodshot, un supersoldat amnésique dont la rencontre avec Kay changera la vie à jamais.	112	2020-05-15 00:00:00	\N	3
177	Quantum And Woody Must Die !	VOUS PENSIEZ QU’ILS EN AVAIENT FINI AVEC LEURS CO****IES ?!	\N	Ils sont venus. Ils ont vu… et ils ont emmerdé tout un tas de monde ! Alors forcément, une équipe de mystérieux vilains va chercher à détruire, purement et simplement, la pire équipe de super-héros de tous les temps. Leur première cible : leurs cerveaux ! Mais qui sont ces nouveaux ennemis ? Et ce duo sexy de cambrioleuses avec qui Quantum et Woody semblent fricoter ? Mais surtout… qu’est-il arrivé au Bouc ?	144	2018-01-25 00:00:00	\N	3
167	NINJAK	NINJAK	\N	Colin King était un espion des services secrets britanniques. Nom de code: Ninjak. Désormais à son compte, il est le mercenaire le plus efficace de l’univers Valiant, un assassin expert en arts martiaux et gadgets high-tech. Son job du moment: traquer les sept ombres, une cabale secrète de maîtres shinobi. Mais, confronté à la terrible Roku, la mission va se révéler plus délicate que prévue. Face à cette menace, c’est tout le passé trouble de Ninjak qui va ressurgir..	848	2021-12-10 00:00:00	\N	3
178	THE DELINQUENTS	QUANTUM ET WOODY + ARCHER ET ARMSTRONG : LE TEAM-UP DE TOUS LES DANGERS.	\N	Quantum et Woody forment la pire équipe de super-héros du monde. Archer et Armstrong sont un duo improbable d’aventuriers démêlant les pires conspirations. Une force mystérieuse va réunir ces “héros” totalement irresponsables pour une épopée à travers l’Amérique et ses mythes les plus obscurs. Le quatuor parviendra-t-il à vaincre le Roi des Clochards ? Quels sombres secrets cache la multinationale agricole Mondostano ?	120	2017-05-11 00:00:00	\N	3
85	THE NEW AVENGERS	CHAOS	1	C'est la fin pour les Avengers avec de nombreux morts dans leurs rangs. Une nouvelle équipe pourra-t-elle renaitre de ses cendres ?	200	2010-03-10 00:00:00	\N	1
168	BLOODSHOT	BLOODSHOT	\N	Avant sa renaissance, découvrez les origines de l'arme parfaite. Tu n’as pas de nom. Ils t’appellent Bloodshot, mais les voix dans ta tête t’appellent “papa”, “monsieur”, “commandant”, “camarade”... Ce que tu as besoin d’entendre pour faire le boulot. Mais après tant de missions et tant de morts, tu es enfin prêt à te confronter à l’organisation qui te manipule, le Projet Rising Spirit, et découvrir qui tu es. Fais vite, car tes anciens maîtres n’apprécient pas lorsqu’une arme de plusieurs milliards de dollars se rebelle. Et où que tu ailles, la mort et la désolation te suivront...	904	2018-04-27 00:00:00	\N	3
179	RAI	BIENVENUE AU NÉO-JAPON	1	Nous sommes en l’an 4001. Le Japon, par la volonté de son leader, l’intelligence artificielle nommée “Père”, est désormais une station orbitale gigantesque surplombant une Terre ravagée. Les millions d’habitants du Néo-Japon sont protégés par un gardien solitaire : Rai. On dit qu’il peut apparaître instantanément. On dit de lui qu’il est un esprit... le fantôme du Japon millénaire. Mais lorsque le premier meurtre commis en mille ans menace de déstabiliser l’omnipotence de Père, Rai est confronté au véritable visage de la nation qu’il défend... Et à sa propre humanité perdue.	352	2016-11-10 00:00:00	\N	3
86	THE NEW AVENGERS	SECRETS ET MENSONGES	2	Les Nouveaux Vengeurs tentent d'élucider le mystère autour de Sentry, un redoutable héros qui a joué un rôle important dans le passé. Quand toute la lumière aura été faite, Sentry s'avérera-t-il un allié solide ou un ennemi implacable ? On assiste également aux débuts de la dernière recrue des Nouveaux Vengeurs, Ronin, et on découvre enfin la vérité sur Spider-Woman.	180	2009-04-01 00:00:00	\N	1
87	THE NEW AVENGERS	RÉVOLUTION	3	\N	265	2010-03-01 00:00:00	\N	1
169	ARCHER AND ARMSTRONG	ILS VONT BOTTER LES FESSES DES ILLUMINATIS !	\N	Obadiah Archer est un jeune idéaliste, endoctriné dans la secte de ses parents adoptifs et entraîné à devenir un assassin sans pitié à leurs ordres. Sa mission : tuer le seul être empêchant ses parents d’accomplir leur plan de domination du monde. Celui qu’ils nomment parfois Satan ou l’Antéchrist, celui qui se fait appeler aujourd’hui... Armstrong : un colosse immortel parcourant le monde depuis des milliers d’années (dont quelques-unes sobres). Un hédoniste amateur de poésie, de femmes et de spiritueux, passant d’aventure en aventure aussi facilement que d’un bar à l’autre... Archer réalise vite que les véritables ennemis sont dans la secte qui l’a élevée... Et il va s’allier à Armstrong pour sauver l’humanité de cette menace ! Ensemble, ils voyageront aux quatre coins du monde et du temps pour déjouer des complots planétaires et lever le voile sur les plus grands mythes de notre histoire. Archer and Armstrong est un buddy movie sensible et haletant, une satire hilarante, une réflexion existentielle...	832	2018-10-19 00:00:00	\N	3
180	RAI	L'ULTIME COMBAT DE RAI.	\N	Cent ans dans le futur, Père, l’intelligence artificielle qui dirige le Japon, devient conscient. Pour protéger ses frontières, il prend la décision drastique de propulser le Japon dans l’espace, où son peuple pourra s’épanouir isolé d’une planète surpeuplée et polluée. Au cours des siècles, orbitant autour d’une Terre de plus en plus instable, le Néo-Japon devient une société modèle, basé sur un idéal de paix, de prospérité… et sur le contrôle total de Père. Mille ans dans le futur, Père créé le premier Rai, conçu pour défendre le Néo-Japon et jurant de le protéger contre toute menace. Pendant des siècles, chaque nouveau Rai va assurer seul l’ordre et la justice… et servir Père aveuglément.	320	2017-06-22 00:00:00	\N	3
119	AVENGERS	LE DERNIER INSTANT BLANC	2	\N	100	2014-11-05 00:00:00	\N	1
170	ARCHER AND ARMSTRONG	A+A : LES AVENTURES D'ARCHER ET ARMSTRONG	\N	Armstrong est un aventurier immortel qui a traversé les 6000 dernières années à boire et faire la java avec les plus grands fêtards que le monde ait jamais connus. Archer est un adolescent expert en arts martiaux et tireur d’élite qui a été élevé dans un unique but : abattre le mal incarné. Petit bémol, ce démon éternel n’est autre qu’Armstrong (qui s’avère être plutôt sympa… quand on apprend à le connaître). Nos deux héros ont depuis pris la route ensemble, et sont même devenus amis et partenaires. Aujourd’hui, Archer s’apprête à partir pour une mission périlleuse. Une quête à l’intérieur de la besace sans fond d’Armstrong pour libérer son ami des griffes du dieu fou Bacchus ! Armstrong s’est retrouvé bloqué dans la besace tout seul en voulant récupérer une bouteille de whisky qu’il avait perdue. Il faut le comprendre, là-dedans c’est un peu comme l’entrepôt Amazon des trésors magiques… et il n’a pas vraiment de plan des lieux. Emprisonné depuis des siècles dans le sac, Bacchus est maintenant à la tête d’une légion de monstres, de gobelins et de golems qui ne rêvent que de s’échapper dans le monde réel et se venger de leur ravisseur… Archer parviendra-t-il à vaincre la représentation divine de l’ivresse et à secourir son meilleur ami ?	336	2017-08-24 00:00:00	\N	3
181	ETERNAL WARRIOR	LA COLÈRE DU GUERRIER ETERNEL	\N	Gilad Anni-Padda est le Guerrier Éternel. Depuis 5000 ans, il fait tomber des empires et brise des armées au service de la Terre. Il en est le gardien et y puise son immortalité. Lorsqu’il est finalement vaincu, se sacrifiant pour sauver la planète, cette dernière lui accorde enfin le repos. Il peut alors retrouver les êtres chers avec qui il a partagé sa si longue vie, dans un paradis, son paradis... Mais il refuse d’abandonner la Terre aux jeunes héros qui la peuplent. Pour retrouver le monde des vivants, il devra laisser derrière lui sa défunte famille dans le havre de paix qui lui est destiné, traverser les profondeurs infernales... et bien pire encore.	392	2017-06-22 00:00:00	\N	3
171	FAITH	FAITH	\N	Orpheline depuis son enfance, Faith Herbert a toujours rêvé d’être une super-héroïne, comme celles des comics, des séries et des films avec lesquels elle a grandi. Lorsqu’elle découvre qu’elle est une psiotique, un être doué de pouvoirs extraordinaires, elle peut enfin réaliser son rêve. Timide journaliste web le jour, elle sillonne la nuit le ciel de Los Angeles pour combattre l’injustice. Lorsque des jeunes psiotiques comme elle disparaissent sans laisser de traces, Faith n’a pas d’autre choix que de passer à l’action !	416	2019-09-27 00:00:00	\N	3
182	IVAR TIMEWALKER	« VENEZ AVEC MOI SI VOUS VOULEZ MARQUER L’HISTOIRE »	\N	En ce moment même, à Genève, l’Histoire est en train de s’écrire. À cent mètres de profondeur, dans le Grand Collisionneur de Hadrons, au CERN, la physicienne Neela Sethi est sur le point de découvrir le voyage dans le temps... Et mettre sa vie en danger par la même occasion. Elle ne le sait pas encore, mais dans dix minutes, des chrononautes bons à rien et de prétendus conquérants de l’espace-temps viendront frapper à sa porte pour la mettre hors d’état de nuire. Heureusement, le légendaire Ivar, le marcheur temporel, arrive à temps pour la sauver... et lui faire entrevoir son destin. Ensemble, ils vont parcourir le temps dans une course contre la montre pour sauver passé, présent et futur de la menace ultime d’Oubli-1... Découvrez Ivar, le troisième frère Anni-Padda apparu dans les pages d’Archer & Armstrong, dans ce récit complet qui vous fera changer d’idée sur les voyages temporels.	328	2017-03-09 00:00:00	\N	3
139	NOVA	CARTE DE MEMBRE	5	Nova affronte un Hulk devenu fou lors d'Avengers et X-Men : AXIS. C'est un combat qui demandera à Sam Alexander de se surpasser. Et après cet affrontement, pas de répit pour l'adolescent qui va recevoir la visite de Carnage.	128	2017-04-19 00:00:00	\N	1
172	HARBINGER	DEUX ÊTRES AUX POUVOIRS EXTRAORDINAIRES, DEUX GÉNÉRATIONS QUI S’OPPOSENT, ET LE SORT DU MONDE ENTRE LEURS MAINS.	\N	Peter Stanchek est un jeune psiotique, doté depuis la naissance de pouvoirs télépathiques immenses. Il vit caché, accro aux drogues qui inhibent des capacités qu’il ne comprend pas. Il est alors recueilli par Toyo Harada. Psiotique lui aussi, survivant d’Hiroshima, il est devenu grâce à ses pouvoirs l’un des hommes les plus riches et influents du monde. Au sein de la très secrète Fondation Harbinger, il recrute une armée de psiotiques. Son but : réaliser son utopie, à n’importe quel prix. En découvrant les méthodes brutales de son mentor, Peter s’enfuit. Il va désormais tout faire pour détruire la Fondation Harbinger et révéler au monde les secrets d’Harada. Avec son amie Kris et les psiotiques Faith, Torque et Flamingo, des jeunes aussi perdus que lui, ils forment les Renégats. Le conflit qui va les opposer à Harada décidera du destin de l’humanité…	928	2018-06-26 00:00:00	\N	3
1	ULTIMATE SPIDER-MAN	POUVOIRS ET RESPONSABILITÉS	1	Un jeune étudiant va voir sa vie bouleverser par la morsure d'une araignée transgénique. Il sera frappé par la tragédie et traqué par les pires super-criminels de toutes parts. Mais il a conscience que de grands pouvoirs impliquent de grandes responsabilités...	992	2021-08-20 00:00:00	\N	1
2	ULTIMATE SPIDER-MAN	HOLLYWOOD	2	Peter Parker poursuit sa scolarité au lycée et découvre la gentillesse de Kitty Pryde, membre des X-Men. Vous assisterez aussi à la naissance de Carnage ainsi qu’aux premières apparitions de Moon Knight, d’Iron Fist, du Docteur Strange et de Shang-Chi dans cette réalité alternative !	1152	2022-01-05 00:00:00	\N	1
3	ULTIMATE SPIDER-MAN	ULTIMATUM	3	Les défis s'enchaînent pour la version Ultimate de Peter Parker et pour son alter ego Spider-Man. Deadpool et Morbius entrent en scène et Peter se voit confronté à sa saga du Clone, une aventure qui laissera des traces. Et quand Magnéto se dresse face à l'humanité, Spidey doit choisir son camp  ! Ce sera celui des X-Men et de son amie Kitty Pryde !	1184	2022-08-31 00:00:00	\N	1
4	ULTIMATE SPIDER-MAN	LA MORT DE SPIDER-MAN	\N	New-York se remet doucement d’un désastre qui a bien failli l’anéantir. Désormais, Spider-Man est aimé de tous, ce qui n’empêche cependant pas Peter Parker d’avoir des ennuis au lycée, dans ses petits boulots et dans son rôle de Spidey ! Si notre Tisseur est habitué à des ennemis comme Mystério, il en existe un, plus terrible que les autres, qui pourrait bien signer la dernière bataille du héros...	648	2021-06-09 00:00:00	\N	1
5	ULTIMATE SPIDER-MAN	COMPENDIUM	\N	Dans l'univers Ultimate non plus, Spider-Man n'est pas le seul héros ! Suivez Peter Parker tandis qu'il croise les autres super-héros de ce monde. C'est l'occasion chaque fois de découvrir des versions un peu différentes de celles que nous connaissons. Puis, les héros Ultimate s'allient pour affronter les plus grandes menaces, comme les Sinister Six et le Docteur Fatalis !	944	2023-02-22 00:00:00	\N	1
7	ULTIMATE SPIDER-MAN	MILES MORALES TOME 2	2	Suite aux évènements de Secret Wars, Miles Morales le Spider-Man de l'univers Ultimate se retrouve sur la Terre-616. Il se porte volontaire pour endosser le costume du tisseur et lutter contre le crime. Il va rapidement prouver qu'il est digne de porter le costume de spider-Man.	856	2023-05-31 00:00:00	\N	1
8	SPIDER-MAN	L'AUTRE	\N	Pourtant habitué à la malchance, Peter Parker coule des jours heureux. Il fait partie des Avengers, vit dans la tour Stark avec son épouse Mary Jane et Tante May. Mais des cauchemars viennent l'assaillir et sa santé se met à décliner : est-ce un effet de son imagination ou s'apprête-t-il à livrer son plus grand combat ? Spider-Man va devoir évoluer ou mourir, car Morlun est de retour.	288	2021-05-19 00:00:00	\N	1
9	SPIDER-MAN	RETOUR AU NOIR	\N	Ayant révélé son identité pendant Civil War, le monde entier sait que Peter Parker est Spider-Man. En conséquence, le super-héros et sa famille vivent alors une période particulièrement ardue. Durant ce moment désespéré, Spidey revêt à nouveau son célèbre costume noir. Il doit stopper de mystérieux criminels opérant sous les traits du Tisseur.	320	2021-09-15 00:00:00	\N	1
10	SPIDER-MAN	LE DERNIER COMBAT	\N	Le pire cauchemar de Peter Parker est devenu réalité : l'un de ses ennemis a appris sa véritable identité et se sert de cette information pour menacer ses proches. La tante de Peter a été kidnappée, Mary Jane est en danger. Une course contre la montre commence, qui confronte Spider-Man à certains de ses pires ennemis pour retrouver sa Tante, préserver son secret et découvrir qui lui veut du mal.	288	2021-01-06 00:00:00	\N	1
11	SPIDER-MAN	VOCATION	\N	Spider-Man est confronté à un nouvel et terrible ennemi, le sanguinaire Morlun qui n'a pour seul but que de le tuer ! Cet énième adversaire est d'autant plus inquiétant que Morlun semble invulnérable et capable de détecter le Tisseur même lorsqu'il n'est pas en costume. C'est le début d'un affrontement qui va pousser Peter Parker dans ses derniers retranchements, et lui faire découvrir de nouvelles choses sur ses pouvoirs.	160	2021-03-17 00:00:00	\N	1
12	SPIDER-MAN	SPIDER-VERSE	\N	Morlun, un des plus redoutables ennemis de Spider-Man est de retour, et cette fois, il est accompagné de toute sa famille. Ensemble, ils sèment le chaos dans le multivers et se nourrissent de la force vitale des différentes incarnations du Tisseur ! Pour espérer survivre, les araignées doivent s’unir, qu’elles viennent de l’univers Amazing, Ultimate, Superior, 1602, 2099... ou de bien d’autres encore !	176	2020-07-01 00:00:00	\N	1
13	VENOM	REX	1	Eddie Brock découvre l’existence de Knull, le dieu des symbiotes. Avec l’aide de Miles Morales, Venom va devoir mener le combat le plus difficile de sa vie. Voici le début de la prestation déjà culte de Donny Cates sur le personnage !	136	2020-08-12 00:00:00	\N	1
14	VENOM	ABYSSE	2	Séparé de Venom, Eddie Brock est seul pour la première fois depuis ce qui lui semble être une éternité... De retour à San Francisco, Brock va tenter de renouer avec son père et apprendra l'existence d'un petit frère dont il n'avait jamais entendu parler !	136	2020-12-30 00:00:00	\N	1
15	VENOM	DÉCHAINÉ	3	Voici quatre récits explorant divers aspects de l'univers de Venom. Carnage et la secte qui tente de ressusciter Cletus Casady après sa mort apparente occupent une place importante du récit, mais nous suivrons aussi les aventures de Venom vues à travers le regard du symbiote lui-même, et découvrirons une histoire terrifiante datant de la guerre du Vietnam.	136	2021-03-03 00:00:00	\N	1
159	DOCTOR STRANGE	LE CRÉPUSCULE DE LA MAGIE	2	Les Sorciers Suprêmes de dimensions parallèles se font tuer les uns après les autres. Lorsque le Dr Strange s'en rend compte, l'Empirikul est déjà en train de détruire la magie à travers l'univers. Les mages de la Terre vont devoir s'adapter... ou mourir.	168	2017-03-08 00:00:00	\N	1
16	VENOM	LA GUERRE DES ROYAUMES	4	Venom a un fils ! Mais protéger le jeune Dylan devient compliqué lorsque les hordes de Malekith menacent d'envahir la Terre, en pleine Guerre des Royaumes. En plus, le Seigneur des Elfes Noirs a décidé de faire du symbiote son arme ! Et pendant ce temps, Carnage continue de s'en prendre à tous ceux qui ont un jour été liés à un symbiote.	136	2021-05-26 00:00:00	\N	1
17	VENOM	ABSOLUTE CARNAGE	5	Après les événements éprouvants de War of the Realms, Eddie Brock trouve enfin le temps de souffler un peu. Mais sans son symbiote, tout est plus compliqué... et particulièrement de protéger son fils Dylan ! D'autant que Carnage s'apprête à plonger toute la ville dans la terreur !	112	2021-08-25 00:00:00	\N	1
67	NOVA	LE ROOKIE	2	Sam Alexander a succédé à son père en devenant le nouveau Nova. Malheureusement, le casque du héros est fourni sans mode d'emploi et l'adolescent doit donc se former sur le tas. Malgré ces difficultés, le jeune homme n'hésite pas à se dresser face aux troupes de Thanos qui attaquent la Terre.	136	2015-09-16 00:00:00	\N	1
121	AVENGERS	INFINITY	4	Tous les Avengers doivent s'unir pour affronter une menace d'ampleur cosmique : les implacables Bâtisseurs. Bien qu'épaulés par les Krees, les Skrulls et Spartax, les héros seront-ils en mesure de les vaincre ? Rien n'est moins sûr..	120	2015-07-01 00:00:00	\N	1
122	AVENGERS	PLANETE VAGABONDE	5	Les Avengers sont présents sur tous les fronts : les menaces d'envergure planétaire se succèdent et de nouveaux ennemis ne cessent de faire leur apparition. Mais une surprise de taille attend nos héros puisque leurs prochains adversaires ne sont autres que les... Avengers !	115	2016-01-06 00:00:00	\N	1
18	VENOM	VENOM ISLAND	6	Absolute Carnage est fini, mais Eddie Brock n'en a pas terminé avec les ennuis ! Pour sauver son fils Dylan, il a libéré Knull, le Dieu des Symbiotes. En plus, le symbiote de Carnage a décidé de prendre possession d'Eddie pour se venger de la mort de Cletus Kasady. Venom l'emmène alors sur une île déserte pour mener contre lui un ultime combat ! Donny Cates poursuit son séjour passionnant sur la série Venom. En mettant en avant Eddie Brock comme personne auparavant, il a fait du symbiote, un personnage incontournable de l'univers Marvel. Le scénariste est rejoint par un habitué des personnages arachnéens : Mark Bagley.	136	2022-01-12 00:00:00	\N	1
19	VENOM	AILLEURS	7	Voilà des mois que le Créateur, cette version de Reed Richards venue d'un monde parallèle, se passionne pour les symbiotes. Aujourd'hui, nous allons enfin en découvrir la raison. Mais quand cette nouvelle menace risque de détruire tout ce à quoi Eddie Brock tient, y compris son fils, Venom n'a pas l'intention de rester passif !	136	2022-04-13 00:00:00	\N	1
20	VENOM	KING IN BLACK	8	Que va-t-il arriver à Eddie Brock, maintenant qu'il est confronté au Dieu des Symbiotes, le terrifiant Knull ? Alors que le Roi en Noir ravage la Terre, personne n'est plus concerné par l'invasion que Venom, car la vie de son fils Dylan est en jeu. Venom survivra-t-il à la confrontation, ou devra-t-il faire le sacrifice ultime ?	176	2022-11-02 00:00:00	\N	1
21	CARNAGE	LE ROI DE SANG	\N	Le Dieu Knull a fait de Carnage son héraut... Touts les héros ayant été infectés par le symbiote sont maintenant en grand danger ! Un affrontement terriblement sanglant va bientôt avoir lieu, spider-Man et Venom ne pourront pas mener seuls ce combat. Les Avengers auront aussi leur rôle à jouer dans cette bataille.	192	2021-08-25 00:00:00	\N	1
22	KING IN BLACK	KING IN BLACK	\N	Les ténèbres envahissent l'univers Marvel alors que Knull et son armée débarque sur Terre ! C'est à Eddie Brock qu'il revient de mener la lutte, aux côtés de tous les héros de l'univers Marvel !\r\n\r\nIl faudra l'ensemble des héros pour arrêter Knull qui compte bien lâcher l'obscurité sur le monde !	1568	2023-08-23 00:00:00	\N	1
23	IRON MAN	LES CINQ CAUCHEMARS	\N	Super-héros, industriel milliardaire, playboy et directeur du S.H.I.E.L.D., Tony Stark n'a pas l'occasion de s'ennuyer, que ce soit sous son identité civile ou lorsqu'il revêt l'armure d'Iron Man. Ezekiel Stane profite de la situation pour lancer sur la compagnie de Stark une O.P.A. d'un genre très hostile !	184	2023-03-01 00:00:00	\N	1
24	IRON MAN	EXTREMIS	\N	Extremis marque le début d'une nouvelle ère pour Tête de Fer. Une terrifiante technologie nommée Extremis fait son apparition et menace désormais la survie de l'humanité. Tony Stark, alias Iron Man, est-il en mesure d'enrayer les conséquences désastreuses de cette invention ?	168	2020-12-30 00:00:00	\N	1
25	WOLVERINE	OLD MAN LOGAN	\N	Dans un monde futuriste, dévasté par les pires criminels, les États-Unis ont perdu leur splendeur. Suite à la victoire des super-vilains, les frontières ont été redéfinies et la population dépérit, abandonnée par ses nouveaux dirigeants. Quand le concept même de héros n’est plus qu’un lointain souvenir, au même titre que l’espoir et la dignité, que peut-il advenir de l’humanité ? Dans le désert de Californie, une terre désolée, contrôlée par le gang de Hulk, Logan veut vivre en paix auprès de sa femme et de ses enfants. Loin de son passé au sein des X-Men, l’ancien héros n’aspire désormais qu’à une paisible vie de famille...	232	2020-07-01 00:00:00	\N	1
26	WOLVERINE	LA MORT DE WOLVERINE	\N	Logan est privé de son facteur auto-guérisseur. Ses ennemis le savent et il est devenu une cible et, pire, un mort en sursis. Alors qu'il sait que le temps qui lui reste est compté, Wolverine réussira-t-il à donner un sens à sa mort ?	496	2019-08-07 00:00:00	\N	1
27	WOLVERINE	ENNEMI D'ÉTAT	\N	Au cours d'une aventure d'apparence classique, Wolverine est confronté à un nouveau vilain, le terrible Gorgone, qui parvient... à le tuer ! Logan se voit alors ressuscité et soumis à l'organisation criminelle la Main, qui se sert de lui pour décimer les héros Marvel. Ses anciens camarades vont s'allier pour le contrer mais pourront-ils se résoudre à tuer définitivement leur ami ? Et s'ils sont convaincus, sont-ils capables de le tuer ?	352	2021-11-27 00:00:00	\N	1
28	HULK	PLANÈTE HULK	\N	Les héros de l’univers Marvel ont décidé d’exiler Hulk loin de la Terre afin qu’il ne soit plus une menace pour les humains. Le Géant de Jade devait donc rejoindre une planète inhabitée et paradisiaque, malheureusement un dysfonctionnement a envoyé le colosse sur une terre inhospitalière, où Hulk devient un gladiateur au service de l’amusement des puissants... Mais le guerrier va bientôt rejoindre la rébellion locale et devenir le héros de tout un peuple ! Avec le Silver Surfer en invité d’honneur.	400	2020-10-14 00:00:00	\N	1
29	HULK	WORLD WAR HULK	\N	Après plusieurs années passées sur Sakaar (Planète Hulk), Hulk revient de son exil... et il est énervé ! Fort de ses connaissances acquises au fin fond de la galaxie, le Géant Vert parvient à rentrer sur Terre pour se venger des héros l'ayant envoyé dans l'espace... et les conséquences seront terribles.	264	2022-05-25 00:00:00	\N	1
30	MAESTRO	FUTUR IMPARFAIT	\N	Hulk est en paix avec Banner et ils ont adopté une forme commune, celle d'un Hulk intelligent. Mais lorsqu'il est projeté dans un futur impossible, Hulk s'aperçoit à quel point il ne sera jamais débarrassé du monstre qui est en lui. Il découvre en effet un monde apocalyptique, sur lequel règne un tyran nommé le Maestro, qui n'est autre que... lui-même !	168	2023-05-03 00:00:00	\N	1
31	MAESTRO	WORLD WAR M	\N	L'A.I.M., le Panthéon... (même le Docteur Fatalis !) sont tombés face à la puissance du Maestro ! Mais le Titan de Jade ne devrait pas s'asseoir trop vite sur son trône, car l'Abomination est de retour d'entre les morts et pourrait bien représenter la pire des menaces pour son règne. D'autant qu'il a recruté de puissants alliés !	128	2022-11-16 00:00:00	\N	1
47	THOR	WAR OF THE REALMS	\N	La Guerre des Royaumes menée par l'Elfe Noir Malekith a mis les neuf royaumes à feu et à sang, et maintenant c'est au tour de Midgard, la Terre, de subir son assaut ! Tous les héros vont devoir s'unir s'ils veulent repousser l'armée de l'envahisseur, mais quelles chances ont-ils quand Thor manque à l'appel ? Une équipe de héros triés sur le volet va être envoyée à sa recherche.	320	2021-06-09 00:00:00	\N	1
32	HULK	SYMPHONIE EN GAMMA MAJEUR	\N	Comment Hulk est-il devenu l'être maléfique et sans pitié qui domine la Terre et se fait donner le nom de Maestro ? Qu'est-il arrivé à la planète et aux autres héros ? Comment Rick Jones a-t-il réuni toutes les armes et les souvenirs des super-héros tombés ? Et qu'est-il arrivé au vrai Hulk ?	128	2021-08-25 00:00:00	\N	1
33	MAESTRO	GUERRE & PAX	\N	Le Maestro a renversé le précédent souverain de Dystopia, mais il ne compte pas s'arrêter en si bon chemin. Il est temps que la planète entière le reconnaisse comme son unique Dieu ! Mais celui qu'on appelait autrefois Hulk n'est pas le seul immortel sur Terre et le Panthéon va se dresser face à lui. Quel vilain bien connu se cache derrière cette équipe ?	128	2022-01-12 00:00:00	\N	1
34	FANTASTIC FOUR	FANTASTIC FOUR PAR WAID ET WIERINGO	\N	Des insectes géants ! Des équations vivantes ! Johnny Storm en chef d'entreprise ! Des molécules instables explosives ! Vivez les aventures les plus folles des Quatre Fantastiques, avant de basculer dans l'horreur lorsque le Docteur Fatalis se décide à commettre l'impensable. Pour sauver l'un des leurs, les Fantastiques devront vaincre la mort elle-même ! Nous avons recueilli dans cet album tous les épisodes réalisés par Mark Waid (Daredevil, Captain America) et le regretté Mike Wieringo. Il s'agit d'une période extrêmement populaire dans la carrière de la Première Famille de l'univers Marvel, avec des sagas laissant beaucoup de place à l'humour et à l'amour familial qui unit cette équipe. Un nouveau film Fantastic Four, cette fois orchestré par les Studios Marvel, s'annonce à l'horizon !	864	2022-12-07 00:00:00	\N	1
35	FANTASTIC FOUR	FANTASTIC FOUR PAR HICKMAN TOME 1	1	Comment un héros, aussi brillant soit-il, peut-il résoudre tous les problèmes de la Terre ? La réponse réside peut-être dans le Pont, l'une des dernières inventions de Reed Richards, qui amènera ce dernier à croiser de nombreux individus ayant la même ambition.\r\n\r\nLa rééditon de l'un des runs les plus ambitieux sur les Fantastic Four arrive en 2 tomes chez Panini Comics !	800	2023-03-15 00:00:00	\N	1
6	ULTIMATE SPIDER-MAN	MILES MORALES TOME 1	1	Quand le Peter Parker de l'univers Ultimate tombe, le monde a besoin d'un Spider-Man, c'est alors que Miles Morales intervient ! Mais quel est le secret de ses pouvoirs ? Réussira-t-il à les maîtriser avant que le Scorpion ne fasse des victimes ? Miles apprend par ailleurs la vérité sur son oncle Aaron et Captain America vient lui demander de participer à une guerre ! Vous avez aimé les Marvel Omnibus consacrés à Ultimate Spider-Man ? L'aventure continue avec Miles Morales et Brian Michael Bendis au scénario. Un héros qui revient sur le devant de la scène avec la sortie l'année prochaine de de Spider-Man : Across the Spider-Verse (Part one).	1168	2022-11-16 00:00:00	\N	1
36	FANTASTIC FOUR	FANTASTIC FOUR PAR HICKMANT TOME 2	2	La Guerre des Quatre Cités bat son plein, et la Fondation du Futur se retrouve prise au milieu d'un conflit entre les Inhumains, Annihilus et les Krees. L'arrivée de Galactus pourrait-elle améliorer la situation ? C'est rarement le cas. Et face au Conseil des Reed et aux Célestes Fous, Valeria se tourne vers le pire ennemi des Fantastiques ! Fatalis incarnera-t-il le sauveur ? Au cours des années, Jonathan Hickman s'est imposé comme l'un des meilleurs "architectes" de la Maison des Idées, avec des sagas imposantes, soigneusement conçues et déroulées.	832	2024-03-27 00:00:00	\N	1
37	NEW X-MEN	E COMME EXTINCTION	1	assandra Nova est une nouvelle ennemie des mutants, et en particulier du Professeur Charles Xavier. Pour sa première attaque, elle ne fait pas dans la dentelle puisqu'elle lance de nouvelles Sentinelles sur Génosha, massacrant la quasi-totalité de ses habitants. C'est le début d'une nouvelle ère pour les X-Men qui vont devoir affronter de nouveaux dangers.	576	2018-10-03 00:00:00	\N	1
38	NEW X-MEN	PLANETE X	2	Dans ce deuxième tome de la prestation de Grant Morrison, vous assisterez à une révolte au sein de l’Institut de Charles Xavier, suivie d’un étrange homicide sur lequel va enquêter Bishop. Puis, après que Wolverine, Cyclope et Fantomex se soient penchés sur les mystérieuses origines de Logan et sur le programme Arme Plus, un ennemi des X-Men lance une attaque d’envergure sur New-York.	488	2019-06-12 00:00:00	\N	1
39	X-MEN	LE COMPLEXE DU MESSIE	\N	Depuis House of M, la race des Mutants est au bord de l'extinction. La naissance d'un bébé porteur du Gène-X réveille l'espoir des X-Men, ainsi que les convoitises de leurs ennemis. Cable, Cyclope, les X-Men et X-Force vont tout faire pour que cet enfant survive. Retrouvez la destinée de Hope dans l'intégralité de deux crossovers cultes : Le Complexe du Messie et La Guerre du Messie.	576	2018-10-03 00:00:00	\N	1
40	X-MEN	LE RETOUR DU MESSIE	\N	La population mondiale des homo superior est sur le déclin. Mais lors du Complexe du Messie un nouveau bébé mutant a fait son apparition : Hope. Les X-Men attendent son retour après son long voyage temporel avec Cable. Pour Cyclope, l’adolescente est leur messie, celle qui permettra l’apparition de nouveaux mutants. Malheureusement, les ennemis des héros pensent la même chose. Pour protéger Hope, les X-Men sont prêts à mourir. Et ils vont peut-être devoir le faire !	584	2019-04-10 00:00:00	\N	1
41	X-MEN	SCHISM	\N	Les X-Men vivent une période difficile. Alors que l’opinion publique se méfiait des mutants, un incident international propage une vague de haine, l’intolérance à leur égard atteint son paroxysme. C’est alors qu’une scission survient au sein même des leaders du groupe.	272	2019-09-11 00:00:00	\N	1
42	AVENGERS - X-MEN	AXIS	\N	Crâne Rouge a volé le cerveau du Professeur Xavier et Magnéto, fou de rage, a tué le vilain, provoquant la renaissance du terrible Onslaught… et ça n'est que le début ! Lors d'un affrontement terrible, une vague d'énergie inverse l'éthique de certains personnages importants.	264	2015-02-25 00:00:00	\N	1
43	X-MEN	AVENGERS VS X-MEN TOME 1	1	Quand le Phénix arrive sur Terre, Cyclope entrevoit le prélude à une renaissance de la race mutante alors que Captain America y voit un signe de l’apocalypse. Le conflit est inévitable entre les X-Men et les Avengers. Les meilleurs scénaristes (Bendis, Aaron, Brubaker, Fraction, Hickman...) et les meilleurs dessinateurs (Romita Jr, Coipel, Kubert…) sont aux commandes de ce titre incontournable. Cet album MARVEL DELUXE présente la saga complète, composée d’un prologue et de douze chapitres.	376	2020-01-08 00:00:00	\N	1
44	X-MEN	AVENGERS VS X-MEN TOME 2	2	Le grand événement Avengers vs X-Men se poursuit en MARVEL DELUXE. Des auteurs de talent vous font découvrir les principaux affrontements entre Avengers et X-Men. La Chose contre Namor, Iron Man contre Magnéto, la Panthère Noire contre Tornade... et bien d’autres face-à-face sont au programme ! Retrouvez également trois webcomics ainsi que la mini-série épilogue intitulée Conséquences.	280	2020-01-08 00:00:00	\N	1
45	THOR	RENAISSANCE	1	Ragnarok a entraîné la disparition de tous les dieux et déesses d’Asgard, Thor n’ayant rien pu faire pour empêcher ce désastre. Cependant, ces divinités ont disparu de manière héroïque, brisant à jamais le cycle des résurrections. La nature a toutefois horreur du vide et Mjolnir est retombé sur Terre, attendant qu’un mortel le brandisse de nouveau. Lorsque Donald Blake se saisit du marteau, un nouveau cycle commence et les dieux réapparaissent aux quatre coins du monde...	324	2020-02-12 00:00:00	\N	1
46	THOR	VICTOIRE	2	Asgard est sur Terre, flottant au-dessus de Broxton. Thor a réveillé tous ses camarades asgardiens de leur long sommeil. Tous ? Non. Il manque encore le premier d'entre eux, Odin, le Père de Tout. Pourquoi Thor n'a-t-il pas encore éveillé son père, alors même qu'il a ramené à la vie son maléfique frère Loki ? Quant aux Asgardiens, ils font connaissance avec leurs nouveaux voisins de la planète Terre !	248	2021-02-24 00:00:00	\N	1
120	AVENGERS	PRELUDE A INFINITY	3	De la Terre Sauvage à Mars, face au Maître de l'Evolution ou à l'A.I.M., les Avengers sont sur tous les fronts. Mais une terrible menace se profile à l'horizon et les plus grands héros de la Terre ne réussiront pas à vaincre les Bâtisseurs sans alliés...	120	2015-04-29 00:00:00	\N	1
48	THANOS	THANOS GAGNE	\N	Thanos est transporté dans un lointain futur où il a l'agréable surprise de découvrir qu'il a triomphé de tous les héros de la Terre et de l'univers. Il rencontre une version plus âgée de lui-même, qui règne en maître absolu, aidé par un Ghost Rider assez... particulier. Cette saga est celle qui a installé Donny Cates parmi les scénaristes incontournables de la Maison des Idées. Elle braque un projecteur sur le nihiliste le plus célèbre de l'univers Marvel. Avec également les débuts du Ghost Rider cosmique !	184	2023-11-02 00:00:00	\N	1
49	THANOS	LE RETOUR DE THANOS	\N	Thanos est de retour, et il a soif de vengeance contre tous ceux qui se sont opposés à lui. Mais ne lui en déplaise, sa famille ne compte pas le laisser faire ! Quel est le plan des adversaires du Titan Fou ? Toute la puissance de la Garde Impériale Shi'ar suffira-t-elle à l'arrêter ?	136	2022-08-17 00:00:00	\N	1
50	THANOS	L'ASCENSION	\N	Les sombres origines de Thanos ! Découvrez comment un amour véritable engendre un torrent de sang et change non seulement le cours de la vie d’un jeune garçon mais aussi le visage de toute la galaxie. Devenu l’égal d’un dieu, le Titan Fou sème la mort dans ce récit captivant où se mêlent tragédie, faux-semblants et destinée.	128	2020-08-05 00:00:00	\N	1
51	THANOS	THANOS IMPERATIVE	\N	Thanos fait savoir qu’il faut compter encore avec lui parmi les puissances cosmiques et qu’il est encore capable de peser sur l’échiquier interstellaire.\r\n\r\nC’est pourquoi les Gardiens de la Galaxie et Nova vont devoir s’allier pour stopper ses ambitions nihilistes. Mais, à cause de la Faille, une déchirure de l’espace-temps, la réalité du cancerverse est menacée…	200	2019-05-22 00:00:00	\N	1
52	THANOS	LE SAMARITAIN	\N	Voulant s’acquitter de sa dette envers l’univers, Thanos a décidé de réparer certains dommages. Pour y parvenir, il doit manipuler de nouveaux adversaires et d’anciens alliés comme Adam Warlock, Galactus, Dragon-Lune et Star-Lord.	288	2018-05-02 00:00:00	\N	1
53	LES GARDIENS DE LA GALAXIE	HERITAGE	\N	Réunis durant Annihilation: Conquest, les Gardiens de la Galaxie s'apprêtent désormais à protéger l'univers ! Retrouvez Star-Lord, Drax le Destructeur, Rocket Raccoon, Gamora, Adam Warlock, Quasar et bien d'autres héros cosmiques dans de fantastiques aventures. Ils sont notamment opposés aux terribles Skrulls !	160	2014-08-06 00:00:00	\N	1
54	LES GARDIENS DE LA GALAXIE	WAR OF KINGS	\N	D'incroyables révélations divisent l'équipe. Que va devenir l'univers si les Gardiens de la Galaxie se séparent ? Et pourquoi Star-Lord disparaît-il mystérieusement ? Découvrez aussi une aventure se déroulant lors du conflit War of Kings entre les Krees et les Shi'ar.	304	2015-04-08 00:00:00	\N	1
55	LES GARDIENS DE LA GALAXIE	COSMIC AVENGERS	1	Alors qu'ils ne sont pas censés s'approcher de la Terre, les Gardiens de la Galaxie vont braver cette interdiction et accueillir un nouveau membre : l'Avenger Iron Man. Ensemble, ces héros doivent protéger Londres qui est le théâtre d'une invasion d'aliens Badoon, rencontrer la mystérieuse Angela puis affronter les armées de Thanos	320	2020-08-19 00:00:00	\N	1
56	LES GARDIENS DE LA GALAXIE	ANGELA	2	Suite à la grande saga Age of Ultron, la mystérieuse Angela apparaît dans l'univers Marvel et croise la route des Gardiens de la Galaxie ! Star-Lord et ses alliés sont ensuite opposés aux armées de Thanos au cours du conflit spatial Infinity.	168	2015-03-04 00:00:00	\N	1
57	LES GARDIENS DE LA GALAXIE	LA FIN DES GARDIENS	3	L'agent Venom et Captain Marvel rejoignent l'équipe des Gardiens de la Galaxie, alors que le groupe vole en éclats. Star-Lord est capturé par son père, tandis que Drax doit affronter le terrible Gladiator.	136	2016-05-04 00:00:00	\N	1
58	LES GARDIENS DE LA GALAXIE	ORIGINAL SIN	4	Comment Star-Lord, Thanos et Drax ont-il fait pour s'échapper du Cancervers? Et où se trouve Nova? une fois ces mystères élucidés, les Gardiens de la Galaxie vont se rendre sur la planète des symbiotes, où de nouveaux dangers les attendent.	129	2016-10-19 00:00:00	\N	1
59	LES GARDIENS DE LA GALAXIE	LES GARDIENS RENCONTRENT LES AVENGERS	5	Trois récits dans ce volume : la fin des Gardiens de la Galaxie, une aventure partagée avec les Avengers face à Nebula, et enfin, une courte histoire où les Gardiens récupèrent un élément essentiel du vaisseau.	111	2017-04-19 00:00:00	\N	1
60	LES ÉTERNELS	SEULE LA MORT EST ÉTERNELLE	1	Une nouvelle ère commence pour les plus anciens super-êtres de la planète. Bien avant les Avengers, bien avant que les humains n'apprennent à se tenir debout, Ikaris, Circé, Thena et les autres Éternels étaient déjà là, avec pour principale mission de protéger la Grande Machine que nous appelons la Terre. Mais aujourd'hui, quelqu'un tue les Éternels et la Machine ne parvient plus à les ressusciter ! Thanos est de retour, mais il n'agit pas seul. Y aurait-il un traître parmi les Éternels ?	152	2021-11-03 00:00:00	\N	1
61	LES ÉTERNELS	GLOIRE À THANOS	2	Les Éternels savent maintenant qu'à chaque fois qu'ils sont ressuscités, un humain meurt. Comment peuvent-ils survivre à cette révélation ? Mais ont-ils le choix ? La situation peut-elle encore se détériorer à cause de Thanos ? Au sein de la ville des Déviants, les héros doivent prendre des décisions cruciales.	144	2022-09-14 00:00:00	\N	1
62	LES ÉTERNELS	UNE HISTOIRE ÉCRITE DANS LE SANG	3	Les deux premiers volumes de la série ne vous ont pas dévoilé tous les secrets des Éternels ! Nous suivons Thanos lorsqu'il était le chef de la race d'immortels et découvrons qu'il n'a pas été le pire leader des Éternels ! Qui est Uranos et pourquoi ne faut-il surtout pas qu'il soit libéré ? L'occasion également de revenir sur les origines du Titan Fou, et de s'intéresser à deux personnages qui évoluent en marge de leurs semblables : Ajak et Makkari.	104	2023-02-15 00:00:00	\N	1
63	LES ETERNELS	BRAVER L'APOCALYPSE	\N	L'existence des Éternels est maintenant connue de tous et pour ceux qui auraient du mal à le croire, un Céleste gigantesque est désormais posté en plein San Francisco. Mais Ikaris, Thena, Makaari et les autres sont-ils des amis ou des ennemis ? Ou les deux peut-être ? Iron Man, directeur du S.H.I.E.L.D., aimerait bien avoir la réponse. Quant aux X-Men qui vivent justement à San Francisco, ils pourraient eux aussi avoir leur mot à dire !	264	2021-11-03 00:00:00	\N	1
64	NOVA	ANNIHILATION CONQUEST	1	Avant la vague d'annihilation, le Nova Corps protégeaient la galaxie. Désormais, il ne reste plus que Richard Rider, le dernier des Nova. Seul, il va devoir affronter les plus terribles adversaires dont le Phalanx.	300	2016-10-05 00:00:00	\N	1
65	NOVA	SECRET INVASION	2	Richard Rider est le dernier membre du Nova Corps, il a une galaxie entière à défendre et il ne sait pas forcément qui sont ses alliés. Cest donc seul quil doit sauver la population dune planète menacée par Galactus et son héraut, le Silver Surfer, ou répondre à la surprenante demande daide du Super-Skrull.	220	2017-06-28 00:00:00	\N	1
66	NOVA	ORIGINES	1	Découvrez les origines de Nova ! Un jeune garçon prénommé Sam Alexander réalise que les histoires du Nova Corps sont bel et bien réelles. Héritant du casque de Nova, il va se trouver au cour d'un conflit intergalactique et croiser la route des Gardiens de la Galaxie.	136	2014-11-05 00:00:00	\N	1
68	DAREDEVIL	UNDERBOSS	1	Matt Murdock est Daredevil ! Qui a lâché le morceau et pourquoi ? Les conséquences vont être terribles pour l'Homme Sans Peur, dont l'identité secrète est dévoilée aux yeux de tous et qui n'a plus d'autre choix que de nier, ou d'avouer ! Tout ça bien sûr en continuant de défendre la justice, dans la rue et au tribunal ! Et comment va réagir son entourage proche, en particulier Foggy Nelson ? La réédition très attendue du Daredevil de Brian Michael Bendis et Alex Maleev. Peut-être LA période la plus importante du justicier aveugle.	280	2021-08-25 00:00:00	\N	1
69	DAREDEVIL	LE PETIT MAÎTRE	2	Lorsque le Tigre Blanc est accusé du meurtre d'un policier et qu'il a besoin d'un avocat pour se défendre, il se tourne vers Matt Murdock. Mais Daredevil a déjà beaucoup à faire entre son identité secrète qui a été dévoilée, le Hibou qui tente de faire main basse sur la pègre new-yorkaise et Mary Typhoïde qui fait son grand retour.	312	2021-12-08 00:00:00	\N	1
70	DAREDEVIL	LE ROI DE HELL'S KITCHEN	3	Daredevil s'est autoproclamé Caïd de New York après avoir violemment renversé Wilson Fisk. Un an plus tard, le héros a fait le ménage à Hell's Kitchen mais cela n'a fait que déplacer le problème. Il va devoir rendre des comptes à ses pairs, tandis que des ninjas s'en prennent directement à son alter ego Matt Murdock.	256	2022-08-24 00:00:00	\N	1
71	DAREDEVIL	LE RAPPORT MURDOCK	4	Le Caïd, chef de la pègre new-yorkaise, a enfin été vaincu par Daredevil. Après cela, l'Homme sans Peur a pris sa place pour éviter que l'empire de son ennemi de toujours ne soit récupéré par un autre malfrat. Mais à présent, Matt Murdock ne doit pas seulement craindre un vieux boss à peine sorti de prison, mais également le chef du FBI qui veut le coffrer pour son activité de justicier !	408	2023-01-25 00:00:00	\N	1
72	DAREDEVIL	DAREDEVIL JAUNE	\N	Revivez les débuts de Daredevil, lorsque Matt Murdock découvre ses pouvoirs et porte son costume jaune et noir. De la mort de son père Battlin' Jack Murdock à son affrontement avec le redoutable Homme Pourpre, en passant par sa rencontre avec la belle Karen Page, c'est une nouvelle vision des origines du justicier aveugle qui vous sont proposées.	152	2021-05-05 00:00:00	\N	1
73	DAREDEVIL	SOUS L'AILE DU DIABLE	\N	Karen Page quitte Matt Murdock car elle a un secret qui pèse sur sa conscience... Une jeune fille de quinze ans laisse à Daredevil la garde de son bébé qui est peut-être l'Antéchrist, alors que Foggy Nelson est accusé de meurtre. Et ce n'est que le début de la déscente aux enfers de l'Homme sans peur ! Qui est le mystérieux adversaire qui tire les ficelles en coulisses ? Découvrez-le dans ce récit incontournable, signé Kevin Smith et Joe Quesada.	192	2022-01-12 00:00:00	\N	1
74	DAREDEVIL	RENAISSANCE	\N	Karen Page, l'ancien amour de Matt Murdock devenue toxicomane, a commis l'impensable en échangeant l'identité secrète de Daredevil contre une dose. Le terrible secret tombe entre les mains du Caïd, qui va méticuleusement détruire tout l'univers de Murdock. L'Homme Sans Peur pourra-t-il se relever de cette épreuve ?	176	2021-09-22 00:00:00	\N	1
75	DAREDEVIL	L'HOMME SANS PEUR	\N	Suite à un accident qui lui a coûté la vue, l’étudiant en droit Matt Murdock est doté de pouvoirs qu’il a l’intention de mettre à profit pour faire le bien. Découvrez comment Matt Murdock devient Daredevil. Un chef-d’oeuvre signé Frank Miller et John Romita Jr.	160	2020-08-05 00:00:00	\N	1
76	DEADPOOL	IL FAUT SOIGNER LE SOLDAT WILSON	\N	Découvrez les origines de Deadpool et de l'équipe de mercenaires dont il faisait partie, avec Bullseye, Silver Sable et Domino. Mais comme c'est Wade Wilson lui-même qui raconte son histoire devant une commission spéciale du sénat des États-Unis, peut-on vraiment y croire ?	112	2021-06-16 00:00:00	\N	1
77	DEADPOOL	DEADPOOL MASSACRE MARVEL	\N	Et si Deadpool devenait un tueur en série de super-héros et de super-vilains ? S’il décidait un jour de tous les supprimer de l’univers Marvel, comment pourrait-on stopper cette croisade meurtrière ? Le mercenaire disert est plus dérangé que jamais dans ce récit où l’horreur remplace l’humour.	104	2020-08-05 00:00:00	\N	1
78	DEADPOOL	DEADPOOL RE-MASSACRE MARVEL	\N	Deadpool est prêt à massacrer de nouveau l'intégralité de l'univers Marvel. Quitte à réitérer l’exercice, autant faire preuve d'originalité ! Vous allez découvrir de nouvelles façons de tuer les héros, et vous pouvez faire confiance au Mercenaire Disert : il est très créatif ! Par les auteurs de Deadpool massacre l'univers Marvel.	112	2018-10-03 00:00:00	\N	1
137	NOVA	LA ROUTE VERS NULLE PART	3	Sur la piste des membres disparus du Nova corps, Nova parvient à sauver la vie de certains de ses nouveaux amis. Mais tandis que Sam enquête sur la disparition de son père, il se fait un nouvel ennemi. Comme si cela ne suffisait pas, Beta Ray Bill arrive sur Terre avec la volonté d'en découdre.	152	2016-03-02 00:00:00	\N	1
79	DEADPOOL	DEADPOOL MASSACRE DEADPOOL	\N	Après avoir exécuté les héros Marvel ainsi que ceux de la littérature, Deadpool décide de s’en prendre à lui-même ! Wade Wilson découvre en effet l’existence d’une infinité de mondes au sein du multiverse et la présence d’une infinité de Deadpool différents dont Têtepool, Lady Deadpool, Pandapool... Va-t-il tous les tuer et anéantir l’univers ?	96	2016-02-03 00:00:00	\N	1
80	DEADPOOL & CABLE	LE CULTE DE LA PERSONNALITE	1	Les mercenaires mutants Wade Wilson et Nathan Summers font désormais équipe et forme un duo de choc ! Mais peuvent-ils se supporter et se battre côte à côte sans devenir fous ? Action, aventure, humour, espionnage et multiples coups de feu sont au programme de ce premier album explosif ! Cable et Deadpool vous attendent... armés jusqu'au dents !	312	2014-10-08 00:00:00	\N	1
81	DEADPOOL & CABLE	LEGENDES VIVANTES	2	Deadpool subit un lavage de cerveau ! Va-t-il à son tour devenir un ennemi de l'État comme Wolverine avant lui ? Et pendant ce temps, Cable reste introuvable... Avec Cyrène et Rocket, le mercenaire disert part à la recherche de son coéquipier et se trouve notamment propulsé dans la réalité de House of M. Découvrez aussi les secrets d'Apocalypse.	304	2015-03-04 00:00:00	\N	1
82	DEADPOOL & CABLE	L'EFFET DOMINO	3	Deadpool part au Rumekistan afin d'assassiner un haut dirigeant mais ses plans sont contrecarrés par Domino ! Wade Wilson se retrouve ensuite au coeur de la guerre civile qui divise l'univers Marvel. Il se mesure alors aux Avengers des Grands Lacs et à Daredevil. Pendant ce temps, Cable rencontre en secret Captain America.	304	2015-09-16 00:00:00	\N	1
83	DEADPOOL & CABLE	DEUX MUTANTS ET UN COUFFIN	4	Le mercenaire disert cherche déjà à remplacer son partenaire ! Dans ces aventures délirantes, Deadpool fait équipe avec Bob, l'agent de l'Hydra, mais aussi les X-men, Wolverine, Captain America, Docteur Strange, Ka-Zar, les Quatre Fantastiques, Spider-Man et les Avengers !	304	2016-02-03 00:00:00	\N	1
84	AVENGERS	SECRET WAR	\N	Que peuvent avoir en commun Spider-Man, Wolverine, Black Widow, Daredevil et Captain America ? Seul Nick Fury le sait ! C'est lui qui a recruté ces héros pour mener une mission tellement secrète... qu'ils ont tout oublié de ses détails ! Mais aujourd'hui, le passé rattrape Fury et ses guerriers secrets, et tous ne vont pas apprécier le lavage de cerveau qu'ils ont subi !	192	2022-07-13 00:00:00	\N	1
88	THE NEW AVENGERS	CONFIANCE	4	Les Nouveaux Vengeurs se mettent à l’heure de Secret Invasion ! Au début de ce tome, l’équipe comprenant Wolverine, Spider-Man et Luke Cage s’aperçoit qu’Elektra a été remplacée par un Skrull, une race extraterrestre capable de prendre n’importe quelle forme. Le doute s’installe alors chez les super-héros qui ne savent plus si le costume de leur “collègue” est porté par un vrai héros ou un imposteur. C’est ainsi qu’a commencé Secret Invasion, l’un des plus grands crossovers proposés par Marvel.	300	2011-03-01 00:00:00	\N	1
89	THE NEW AVENGERS	L'EMPIRE	5	La méfiance règne parmi les membres des Nouveaux Vengeurs. Ils ont en effet découvert qu'un grand nombre de Skrulls se cachaient parmi les surhumains. Ils ont compris qu'une attaque contre la Terre se préparait.	264	2012-03-14 00:00:00	\N	1
90	AVENGERS	HOUSE OF M	\N	Wanda la Sorcière Rouge a réécrit la réalité : un nouveau monde a été créé où dominent les mutants et où Magneto en est le roi ! Personne ne se souvient de l'ancienne réalité...Mais Wolverine, maintenant membre de la force de maintien de la paix de Magnus, le S.H.I.E.L.D., se souvient du monde d'avant. Et sa quête pour retrouver et réveiller ses anciens alliés, dont les Avengers et les X-Men,j déclenche une révolution !	1296	2023-11-08 00:00:00	\N	1
91	AVENGERS	ANNIHILATION	\N	La vague d'Annihilation déferle et l'univers tremble. La prison de Klyn est la première à tomber, puis c'est au tour de Xandar et des Novas de subir l'assaut de cette armada implacable. L’un après l'autre, les empires galactiques s'écroulent. Une équipe improbable de héros fait alors son apparition. Dirigé par Nova et Star-Lord, ce groupe peut-il sauver l’univers ?	832	2023-06-28 00:00:00	\N	1
92	AVENGERS	ANNIHILATION CONQUEST	\N	Alors que l’univers est en lambeaux après la guerre menée par Annihilus, une menace tout aussi terrible s’abat sur Hala, le monde des Krees. D’anciens alliés refont équipe pour faire front, mais leurs forces ne sont pas suffisantes face au Phalanx. De nouveaux héros doivent prendre la relève. Retrouvez Nova, Star-lord, Drax, Gamora mais aussi Mantis, Ronan, Rocket et Groot dans une saga cosmique explosive !	584	2019-07-10 00:00:00	\N	1
93	AVENGERS	CIVIL WAR PRELUDE	0	Spider-Man change de costume et le nouveau lui est fourni par Iron Man lui-même ! L’amitié en Tony Stark et Peter Parker déterminera le choix de ce dernier lorsqu’il devra choisir son camp dans le conflit qui se prépare.\r\n\r\nQuand un mystérieux objet venu des cieux s’écrase sur Terre, ce sont les 4 Fantastiques qui mènent l’enquête pendant que Red Rchards participe à la reformation des Illuminati.	296	2015-09-16 00:00:00	\N	1
94	AVENGERS	CIVIL WAR VOLUME 1	1	Tony Stark veut imposer à tous les super-héros de révéler leur identité au Gouvernement, ce qui provoque un schisme d'un violence incroyable et un affrontement fratricide entre deux camps de héros : ceux qui ont accepté de se faire enregistrer auprès du Gouvernement et ceux, devenus hors-la-loi, qui ont refusé et qui se battent au côté de Captain America.	250	2010-04-07 00:00:00	\N	1
95	AVENGERS	CIVIL WAR VOLUME 2	2	Wolverine part en chasse contre Nitro, responsable de l'attentat meurtrier qui est à l'origine de la Guerre Civile des super-héros. Malheureusement, Logan semble avoir sous-estimé son adversaire et pourrait se retrouver dans un état proche de la mort...	200	2010-04-07 00:00:00	\N	1
96	AVENGERS	CIVIL WAR VOLUME 3	3	Après sa capitulation qui a mis fin à la Guerre Civile, Captain America est abttu sous les yeux du public par un mystèrieux tireur... Qui a assassiné la Légende Étoilée au lendemain de sa défaite ? Quelles seront les réactions des principaux super-héros ?	200	2010-04-07 00:00:00	\N	1
97	AVENGERS	CIVIL WAR VOLUME 4	4	Des journalistes tentent de faire la part des choses dans les événements complexes de la Guerre Civile des Super-Héros : que ce soit au sujet de Speedball, le responsable de la crise, ainsi qu’au conflit entre Namor et les forces d'Atlantis.	328	2012-10-31 00:00:00	\N	1
98	AVENGERS	CIVIL WAR VOLUME 5	5	Les X-Men vont-ils choisir un camp ? Les jeunes héros de l'univers Marvel sont également de la partie, avec une rencontre au sommet entre les Young Avengers et les Fugitifs.	296	2023-09-11 00:00:00	\N	1
99	AVENGERS	CIVIL WAR VOLUME 6	6	Découvrez le point de vue de nouveaux personnages sur le conflit qui déchire la communauté super-héroïque. Avec notamment les Quatre Fantastiques en proie à des divisions internes, ainsi qu'Iron Man et le Punisher !	280	2014-08-20 00:00:00	\N	1
138	NOVA	LA VÉRITÉ SUR LES BLACK NOVA	4	Original Sin a permis au jeune Nova de faire des découvertes sur son père disparu. Pour lever le mystère des Black Nova, Sam Alexander se tourne vers Rocket Raccoon, des Gardiens de la Galaxie !	136	2017-01-04 00:00:00	\N	1
100	AVENGERS	CIVIL WAR II	\N	Ulysse, un nouvel inhumain fait son apparition. Il possède le don d'annoncer les catastrophes qui vont se produire. Pour Captain Marvel, suivre ses indications permettrait de sauver des vies. À l’opposé, Iron Man estime qu'il est dangereux de chercher à maîtriser le futur. Des questions se posent alors : doit-on sacrifier la liberté à la sécurité ? Peut-on enfermer des coupables avant qu'ils aient commis les crimes dont ils sont accusés ? Cette divergence d’opinion entraîne les super-héros Marvel dans une nouvelle guerre civile.	288	2017-11-29 00:00:00	\N	1
101	AVENGERS	SECRET WARS	\N	La collision entre la Terre de l'univers 616 et la Terre de l'univers Ultimate signe la fin de toutes les réalités. Mais le Docteur Fatalis est parvenu à sauver un bout de chaque univers, et il règne en maître sur une planète qui flotte dans le néant et où vivent d'innombrables versions des héros Marvel. Les rares rescapés de notre monde pourront-ils renverser Fatalis et recréer le multivers ?	312	2023-01-04 00:00:00	\N	1
102	DARK AVENGERS	RASSEMBLEMENT	\N	À l'issue de Secret Invasion, le monde entier a vu Norman Osborn, le Bouffon Vert, abattre la reine des Skrulls en direct à la télévision. Dans un monde où les héros ont perdu la confiance du public, le pouvoir est donné à l'ennemi juré de Spider Man, qui crée sa propre équipe d'Avengers ! Dans les rôles de Spidey, Wolverine ou Miss Marvel, Osborn recrute Venom, Daken et Opale. Osborn lui même devient Iron Patriot, un mélange d'Iron Man et de Captain America. Arès et Sentry complètent cette équipe violente et instable, chargée de veiller sur le monde.	272	2020-02-21 00:00:00	\N	1
104	AVENGERS	SIEGE	\N	La mégalomanie de Norman Osborn ne connaît pas de limites ! A la tête de la sécurité nationale et des Dark Avengers, l'ancien Bouffon Vert refuse que la cité d'Asgard soit située aux Etats-Unis. Il élabore alors un stratagème pour attaquer la ville de Thor. Pour tous les héros de bonne volonté, le moment est venu de sortir de l'ombre et de se réunir pour protéger les Asgardiens. Mais Osborn a plus d'un tour dans son sac...	200	2021-11-17 00:00:00	\N	1
105	AVENGERS	SECRET INVASION	\N	À qui faire confiance ? C'est la terrible question que se pose tous les héros lorsqu'ils s'aperçoivent que les extraterrestres Skrulls ont envahi la Terre. Capables de changer de forme, les Skrulls ont infiltré les plus grandes instances mondiales, le S.H.I.E.L.D. et même la communauté des super-héros ! Qui parmi les Avengers est un Skrull ? Dans ces conditions, comment repousser l'invasion ?	192	2022-02-09 00:00:00	\N	1
106	AVENGERS	WAR OF KINGS	\N	Suite à Secret Invasion, Flèche Noire souhaite restaurer la force et la grandeur de son peuple. Ainsi, les Inhumains entrent en guerre contre l'Empire Shi-ar que dirige le terrible Vulcan. Quel sera le souverain capable de remporter la victoire ? Découvrez également l'histoire de Chris Powell alias Darkhawk, un humain qui a acquis par hasard une armure extraterrestre et de fantastiques pouvoirs.	344	2014-03-12 00:00:00	\N	1
103	DARK AVENGERS	EXODUS	\N	Rien ne va plus chez les Dark Avengers de Norman Osborn ! L'équipe n'arrive pas à travailler en groupe : les dissensions se multiplient, la Cabale implose et Sentry devient fou ! Le nouveau chef de l'univers Marvel saura-t -il tenir son rôle de leader face à la menace de l'Homme Molécule? Peut-être cèdera-t-il à ses plus vils penchants en attaquant Asgard, le royaume de Thor ? À la suite de Secret Invasion, Norman Osborn a pris le contrôle du S.H.I.E.L.D. et de l'univers super-héroïque Marvel, remplaçant les héros par des versions dépravées d'eux-mêmes.	304	2021-09-29 00:00:00	\N	1
107	AVENGERS	REALM OF KINGS	\N	La confrontation entre Flèche Noire et Vulcan a engendré une faille dans l'espace-temps. La Garde impériale Shi'ar se lance alors dans une dangereuse mission. Mais est-elle en mesure de l'accomplir ? Médusa, la nouvelle souveraine des Inhumains, tente quant à elle de conserver le pouvoir. Elle fait face à des menaces externes ainsi qu'à des complots au sein de sa propre famille !	264	2015-01-07 00:00:00	\N	1
108	AVENGERS	L'AGE DES HÉROS	\N	Suite à la saga Siège, Iron Man décide de reformer les Avengers. Il est temps d'oublier les querelles passées et de penser à l'avenir... Mais quel est-il ? Les plus grands héros de la Terre vont vite le savoir. Leur ennemi de toujours, Kang le Conquérant, vient leur annoncer un sombre futur, dans lequel les enfants de nos héros font face à une situation bien désespérée.	304	2014-01-08 00:00:00	\N	1
109	AVENGERS	TIME RUNS OUT : TU NE PEUX PAS GAGNER	1	Rien ne va plus chez les Avengers : les secrets des Illuminati ont été dévoilés et le schisme entre Steve Rogers et Tony Stark a conduit à la dissolution de l'équipe. Mais les Incursions, ces phénomènes qui font entrer en collision les univers parallèles, continuent. Qui est intervenu pour empêcher la fin de notre univers, quitte à en détruire un autre ? Découvrez la Cabale de Thanos !	256	2021-03-10 00:00:00	\N	1
110	AVENGERS	TIME RUNS OUT : LA CHUTE DES DIEUX	2	Thanos et la Cabale détruisent des versions parallèles de la Terre et leurs univers avec ! Mais que peuvent faire les Avengers alors qu'ils sont désorganisés, brouillés, et que mettre fin au massacre de la Cabale pourrait signer la mort de notre réalité ? Le salut peut-il venir du Docteur Fatalis ?	264	2021-06-09 00:00:00	\N	1
111	AVENGERS	FEAR ITSELF	\N	Lorsqu'elle empoigne un marteau mystique tombé du ciel, Sin, la fille démente de Crâne Rouge, se transforme en héraut du Serpent. Aidé de ses huit Dignes, des héros et des criminels devenus ses servants, le Serpent plonge la planète dans le chaos et les ténèbres, sa puissance augmentant au fur et à mesure que la peur se répand. Captain America, Thor, Iron Man et les autres Avengers doutent d'être en mesure d'arrêter cette terrible menace.	248	2022-04-13 00:00:00	\N	1
112	AVENGERS	INFINITY : LA CHUTE	1	Les Avengers quittent la Terre pour affronter les Bâtisseurs, l’une des civilisations les plus redoutables de l’univers. Profitant de leur absence, Thanos et ses armées de l’Ordre Noir décident alors d’attaquer la planète. Mais que recherche exactement le Titan fou ? Sa quête va le mener jusqu’à la cité volante des Inhumains où il se retrouve face à Flèche Noire. Pendant ce temps, une guerre éclate entre le Wakanda et Atlantis.	336	2019-04-10 00:00:00	\N	1
113	AVENGERS	INFINITY : DANS LA MAIN GAUCHE DE LA MORT	2	Les Avengers quittent la Terre pour affronter les Bâtisseurs, l’une des civilisations les plus redoutables de l’univers. Profitant de leur absence, Thanos et ses armées de l’Ordre Noir décident alors d’attaquer la planète. Mais que recherche exactement le Titan fou ? Sa quête va le mener jusqu’à la cité volante des Inhumains où il se retrouve face à Flèche Noire. Pendant ce temps, une guerre éclate entre le Wakanda et Atlantis.	328	2019-04-10 00:00:00	\N	1
115	AVENGERS	AGE OF ULTRON	\N	Et si Ultron l'emportait sans que les Avengers ne puissent rien faire pour l'arrêter ? Un matin, les héros se réveillent et la bataille est déjà perdue. Ultron a conquis la planète et imposé son règne de métal à la population. Dans un monde où tout est perdu, le seul moyen de battre la créature est peut-être de remonter le temps. Mais le remède est-il pire que le mal ? Et quelle terrible décision va prendre Wolverine ?	296	2023-01-04 00:00:00	\N	1
116	AVENGERS	FEAR ITSELF TOME 1	1	Les super-héros Marvel doivent à nouveau s'allier pour affronter une menace commune, cette année le crossover s'appelle Fear Itself. Le dieu de la peur est de retour, et il sème la terreur dans l'univers Marvel. Deux albums Monster seront consacrés à cet événement qui concerne tous les héros. Vous y découvrirez l'intégralité de la saga Youth in Revolt, qui s'intéresse aux jeunes héros de la série Avengers Academy et vous retrouverez la Veuve Noire dans une aventure en solo et à Paris. De son côté Namor gèrera les répercussions de Fear Itself sur son royaume aquatique dans The Deep. Un album à ne pas manquer pour tout savoir sur le nouveau crossover Marvel.	200	2012-02-15 00:00:00	\N	1
117	AVENGERS	FEAR ITSELF TOME 2	2	Deuxième et dernier volume MARVEL MONSTER consacré à Fear Itself, le crossover Marvel de ce début d'année ! Le dieu asgardien de la Peur fait trembler la Terre entière, les Vengeurs sont dans les cordes, et c'est le genre de situation où tous les héros, même les moins connus, doivent se mobiliser !	200	2012-05-16 00:00:00	\N	1
118	AVENGERS	LE MONDE DES AVENGERS	1	Les Avengers recrutent de nouveaux membres afin d'élargir leur sphère d'influence à un niveau mondial, voire interplanétaire. Leur première mission ls emmène sur Mars mais les secrets du Jardin vont les renvoyer tout droit en Terre Sauvage. Et quand la Garde Impériale shi'ar est battue sur une lune morte, les Avengers traversent la Galaxie pour affronter leurs agresseurs. A la clé, l'origine de l'univers...	122	2014-06-04 00:00:00	\N	1
123	AVENGERS	LE DERNIER AVENGER	6	Le Docteur Strange et Iron Man ont effacé certains événements de la mémoire de Captain America et ne lui ont jamais avoué. Mais, durant Original Sin, les plus infâmes trahisons ont été révélées et Steve Rogers a appris ce qui s'était passé. Le mensonge sera-t-il l'adversaire qui terrassera définitivement les Avengers?	127	2016-03-02 00:00:00	\N	1
124	NEW AVENGERS	TOUT MEURT	1	L’équipe secrète des Illuminati compte dans ses rangs les êtres les plus intelligents et les plus puissants au monde : la Panthère Noire, Iron Man, le Docteur Strange, Flèche Noire, Mr. Fantastique et Namor, le Prince des Mers. Alors que les univers parallèles rentrent en collision, le groupe doit prendre de dramatiques décisions afin de protéger la Terre. Et si les Pierres de l’Infini ne leur étaient d’aucun secours ?	280	2020-02-12 00:00:00	\N	1
125	NEW AVENGERS	UN MONDE PARFAIT	2	Les Illuminati ont réussi à repousser Thanos, mais les incursions d’ennemis venus d’autres dimensions continuent, amenuisant le nombre de solutions pour les repousser. Le groupe de super-héros réussira-t-il à rester soudé face aux décisions déchirantes qu’il va devoir prendre ? Rien n’est moins sûr...	248	2020-09-30 00:00:00	\N	1
126	AVENGERS	ORIGINAL SIN	\N	Uatu a été tué. Le meurtre a eu lieu sur la Lune avec une arme inconnue, capable d'abattre un extraterrestre vivant là depuis des millénaires. Ses yeux ont été dérobés et ce détail macabre soulève une importante question : qui possède désormais les secrets détenus par le Gardien ? C'est le début d'une enquête qui va dévoiler les plus sombres mystères des héros Marvel.	264	2020-11-25 00:00:00	\N	1
127	AVENGERS	ORIGINAL SIN - HULK / IRON-MAN / THOR	\N	Tout au long de son existence, Uatu a suivi un code moral basé sur une observation passive des événements de la Terre. Or, lorsqu’il est assassiné au début d’Original Sin, tous les secrets dont il était le gardien sont dévoilés. Iron Man et Hulk découvrent ainsi qu'un épisode de leur passé lie leurs destinées, tandis que Thor et Loki apprennent qu'Odin a caché l'existence d'un Dixième Royaume. Quelles décisions vont prendre ces quatre héros suite à ces révélations ?	200	2018-04-11 00:00:00	\N	1
128	AVENGERS	SECRET EMPIRE	\N	Captain America est devenu un agent de l'Hydra. Malheureusement, il est aussi à la tête des États-Unis. Trahis par leur plus fin stratège, les Avengers ont perdu la guerre avant même qu'elle ait commencée.	408	2018-11-07 00:00:00	\N	1
129	AVENGERS	WHAT IF ? VOLUME 1	1	Que se serait il passé si Iron Man était mort juste avant Civil War ? Ou s'il avait perdue la bataille ? Et comment l'histoire aurait changée si Wolverine avait tué Hank Pym avant la création d'Ultron dans Age of Ultron ? La guerre entre les Avengers et les X-Men aurait-elle pu se finir autrement ? Autant de questions qui trouvent ici de surprenantes réponses…	248	2018-11-14 00:00:00	\N	1
130	AVENGERS	WHAT IF ? VOLUME 2	2	Les sagas Marvel sont constellées d’événements cruciaux pour les héros. Mais s’ils avaient été différents ? Si d’autres intrigues avaient été choisies par la Maison des Idées ? Si les artistes avaient pris d’autres directions ? L’univers Marvel ne serait pas tel que nous le connaissons. Découvrez comment aurait pu être la vie des Avengers, des Gardiens de la Galaxie, des X-Men, de Spider-Man et d’autres légendes, si les choses s’étaient déroulées différemment.	296	2019-08-07 00:00:00	\N	1
131	AVENGERS	WHAT IF ? VOLUME 3	3	Et si la Sorcière Rouge avait annulé les pouvoirs de tous les surhumains à la fin de House of M ? Si après Civil War, Mary Jane avait reçu une balle ? Si Wolverine avait sauvé son enfant en tuant le Soldat de l’Hiver ? Cet album dissipe nos doutes en nous proposant une autre version de grands évènements qui ont bouleversé l’univers Marvel. World War Hulk, L’Ère d’Apocalypse ou encore Secret Invasion sont également au programme.	296	2020-08-12 00:00:00	\N	1
132	AVENGERS	FOREVER	\N	Un vieil ami des Avengers, Rick Jones, est la cible d’Immortus, le maître du temps. Pour le sauver, les héros doivent s’allier au terrible Kang et entreprendre un voyage qui les conduira de l’époque du Far West jusqu’à la fin des temps !	312	2022-10-19 00:00:00	\N	1
133	X-MEN	DEATH OF X	\N	Death of X est l'introduction à l'événement Marvel : Inhumains VS X-Men.\r\n\r\nPour Cyclope et ses X-Men, la coupe est pleine : les brumes tératogènes des Inhumains ont tué leur dernier mutant. Scott Summers décide que le temps des négociations ou des explications est dépassé. L’heure de la guerre a sonné.	120	2019-03-13 00:00:00	\N	1
134	X-MEN	INHUMANS VS X-MEN	\N	Depuis que les brumes tératogènes, qui donnent leurs pouvoirs aux Inhumains, se sont répandues dans l’atmosphère terrestre, l’espèce mutante est menacée d’extinction. Face à cette situation, les X-Men déclarent la guerre au peuple de New Attilan. Qui sortira vainqueur de ce conflit ?	216	2018-06-06 00:00:00	\N	1
135	AVENGERS	REUNION	\N	Bien des choses ont séparé les trois principaux membres des Avengers : Iron Man, Captain America et Thor. De Civil War à Siege en passant par Secret Invasion, la mort, la trahison et les désaccords, ont brouillé ces frères d'armes. Comment guérir les blessures et inaugurer un nouvel Âge des Héros ?	128	2021-10-27 00:00:00	\N	1
136	AVENGERS	ZONE ROUGE	\N	En moins de deux heures, un nuage pourpre s’est répandu dans les environs du mont Rushmore, inoculant un virus mangeur de chair à la population. Il semble inarrêtable. Appelés à la rescousse, les Avengers tentent d’évacuer les rares survivants tout en cherchant un antidote.	152	2023-09-13 00:00:00	\N	1
140	LES GARDIENS DE LA GALAXIE	REALM OF KINGS	3	Les Gardiens de la Galaxie ont réussi à vaincre le Magus, mais plusieurs d'entre eux sont morts sur le champ de bataille. L'équipe n'est pourtant pas au bout de ses peines : un parasite venu d'une autre dimension prend l'une des leurs, Dragon-Lune, comme hôte. Le sort de la galaxie et de ses Gardiens est alors entre ses mains.	144	2016-08-24 00:00:00	\N	1
141	NOVA	RETROUVAILLES	6	Sam Alexander doit réparer son casque de Black Nova s'il souhaite retrouver son père. Pour accomplir cette tâche, l'adolescent demande de l'aide à tous tes héros qu'il a croisés lors de ses précédentes aventures : Rocket Raccoon, Cosmo, Beta Ray Bill et même l'incroyable Hulk.	128	2017-06-07 00:00:00	\N	1
142	THE INVINCIBLE IRON-MAN	DANS LA LIGNE DE MIRE	1	En Tanzanie, en pleine rue populeuse, trois jeunes descendent d’une 2CV et se transforment instantanément en bombe humaine. Cet acte terroriste marque l’apparition d’une nouvelle arme de destruction massive, une arme qui concentre l’énergie du corps humain avant de la projeter, sur-amplifiée, en direction des cibles visées. Le SHIELD, dirigé par le multi-milliardaire Tony Stark, enquête… Ils finissent par découvrir que la technologie utilisée est dérivées de celle que Stark a imaginé pour produire l’énergie indispensable à l’armure d’Iron Man… et que celui qui a détourné son invention philanthropique n’est autre que le fils d’Obadia Stane…	336	2012-10-03 00:00:00	\N	1
143	THE INVINCIBLE IRON-MAN	DISLOCATION	2	Tony Stark garde dans son cerveau la précieuse liste des super-héros répertoriés suite à la loi de Recensement des Surhumains. Et depuis qu'il est le gardien de la sécurité nationale, Norman Osborn n'a qu'une obsession : qu'on lui ramène la tête de Tony Stark... La chasse à l'homme est lancée !	264	2013-04-24 00:00:00	\N	1
144	THE INVINCIBLE IRON-MAN	DÉMON	5	Afin de vaincre le Serpent, le dieu asgardien de la Peur. Tony Stark est obligé d'abandonner sa vie confortable et d'entrer dans le monde de la magie. En effet, pour acquérir la puissance thaumaturgique nécessaire, une offrande est inévitable. Il paie ainsi le prix demandé et sacrifie sa sobriété. La victoire face au Serpent est complète mais certains héros ont péri durant la bataille. Dans les rangs des victimes. Detroit Steel, le guerrier en armure de Hammer Industries. Depuis, l'entreprise est dirigée par Justine Hammer et sa fille, Sasha Hammer, qui est devenue la nouvelle Detroit Steel. Seule ombre au tableau : son père n'est autre que le Mandarin, le terrible adversaire d'Iron Man.	310	2015-03-11 00:00:00	\N	1
145	THE INVINCIBLE IRON-MAN	FEAR ITSELF	4	Le docteur Octopus désire profiter de la technologie d'Iron Man. Pour cela, il attire l'attention de Tony Stark en menaçant de tuer des millions d'innocents. Iron Man affronte ensuite la Gargouille grise qui transforme tous les Parisiens en statues de pierre.	312	2014-10-08 00:00:00	\N	1
146	THE INVINCIBLE IRON-MAN	STARK RÉSISTANCE	3	Alors que débute l'Âge des Héros, Tony Stark prend un nouveau départ. Et quoi de mieux que les affaires pour y arriver ? Aussi, il décide de créer la société Stark Résistance. Ce retour sur le devant de la scène ne manque pas d'intéresser l'armée, mais Tony n'a pas l'intention de reproduire ses erreurs passées. Sa technologie ne doit plus mettre en danger la vie de citoyens innocents.	256	2014-01-08 00:00:00	\N	1
147	X-MEN : X OF SWORDS	X OF SWORDS 01	1	Est-ce la fin des mutants ? Quand Apocalypse, contre l'avis du conseil de Krakoa, ouvre la porte vers le monde oublié d'Arakko, il déclenche un conflit qui ne pourra se régler que dans un tournoi gigantesque opposant les mutants les plus puissants aux habitants du mystérieux domaine. L'enjeu : rien de moins que la survie de l'espèce ! Qui seront les dix mutants choisis pour représenter les X-Men ?	200	2001-09-15 00:00:00	\N	1
148	X-MEN : X OF SWORDS	X OF SWORDS 02	1	Le tournoi se poursuit qui doit décider du destin des îles de Krakoa et d'Arakko… et de leurs habitants ! Tout le monde est-il prêt à jouer fair-play dans une compétition aux enjeux si gigantesques ?	200	2021-09-15 00:00:00	\N	1
149	X-MEN : X OF SWORDS	X OF SWORDS 03	3	Tous les mutants cités dans la prophétie de Saturnyne ont récupéré leurs épées et sont prêts à se battre dans l'Outremonde, d'où ne reviendront pas ceux qui seront mortellement blessés. Il est temps de découvrir les adversaires des mutants dans ce tournoi…	168	2021-10-06 00:00:00	\N	1
150	X-MEN : X OF SWORDS	X OF SWORDS 04	4	Les champions de Krakoa affrontent ceux d'Arakko, mais le conflit a déjà fait une victime parmi les mutants. Apocalypse devra-t-il affronter sa femme Genesis, avec laquelle il vient tout juste d'être réuni ? La fantastique saga s'achève ici !	168	2021-10-06 00:00:00	\N	1
151	X-MEN	HOUSE OF X / POWERS OF X	\N	Le monde a changé, l’île de Krakoa est désormais le coeur de la nation mutante. D'où viennent ces plantes qui poussent à travers le monde entier et quel est leur lien avec les mutants ? Plongez dans la série qui bouleverse l’univers mutant de manière radicale et magistrale.	448	2021-10-06 00:00:00	\N	1
152	X-MEN	TOME 1 - PAX KRAKOA	1	Un nouveau monde s'est ouvert aux mutants du monde entier, et le futur ne s'est jamais annoncé si lumineux ! Cela ne veut pas dire que les dangers ne menaceront pas de mettre en péril le paradis que les X-Men ont créé sur Krakoa, mais Cyclope veille avec une poignée de mutants triés sur le volet !	312	2022-09-07 00:00:00	\N	1
153	X-MEN	TOME 2 - LE COMMENCEMENT	2	Les mutants de Krakoa se retrouvent impliqués dans une guerre totale entre l'armée des Cotati et l'alliance formée entre les Skrulls et les Shi'ars ! Puis, les préparatifs se mettent en place pour un évènement qui fera date : le premier Gala des Damnés, au cours duquel une nouvelle équipe de X-Men doit être dévoilée.	288	2023-10-11 00:00:00	\N	1
154	X-MEN	INFERNO	\N	Moira MacTaggert connaît l'avenir. Et pour cause, elle a vécu plusieurs vies et a tout retenu des continuités passées. Elle a partagé son savoir avec Magnéto et Charles Xavier et les a aidés à faire de Krakoa un havre de paix pour les mutants. Elle leur a aussi permis de les ressusciter à volonté. Elle n'avait qu'une condition : ne jamais ranimer l'extralucide Destinée. Mais Mystique n'est pas d'accord. Et aujourd'hui, tout va brûler dans les flammes !	224	2024-01-03 00:00:00	\N	1
155	X-MEN	HELLFIRE GALA	\N	Mettez-vous sur votre trente et un car vous êtes invité au plus grand évènement de l'année : le Gala des Damnés. Les membres éminents de la communauté mutante seront présents, vous y retrouverez vos héros préférés, leurs plus proches alliés et leurs pires ennemis ! Une équipe officielle de X-Men va être démocratiquement élue, entre autres évènements cataclysmiques.	384	2023-08-16 00:00:00	\N	1
156	X-MEN	GIANT-SIZE	\N	Cinq histoires complètes ! Une menace force Jean Grey et Emma Frost à travailler ensemble. Diablo s'aventure dans l'inconnu quand Krakoa perd le contact avec des mutants. Magnéto s'habitue à l'idée de devoir collaborer avec les humains. Fantomex retourne au Monde qui l'a vu naître. Enfin, Tornade doit contrecarrer un complot insidieux.	176	2023-02-22 00:00:00	\N	1
157	DOCTOR STRANGE	LE DÉBUT ET LA FIN	\N	Avide de gloire et de fortune, le Docteur Stephen Strange est un chirurgien renommé. Mais il est victime dun terrible accident qui bouleverse son existence. Il va alors suivre les enseignements dun moine tibétain et devenir le plus grand sorcier de la Terre !	144	2016-10-12 00:00:00	\N	1
158	DOCTOR STRANGE	LES VOIES DE L'ÉTRANGE	1	Le Docteur Strange est le Sorcier Suprême : il gère les affaires magiques de la Terre et de toute la dimension. C'est un travail auquel il est habitué mais il y a une des leçons de son Maître qu'il n'a pas retenu : la magie a un prix et si on ne le paye pas régulièrement, les conséquences peuvent être terribles.	111	2016-10-12 00:00:00	\N	1
160	DOCTOR STRANGE	DU SANG DANS L'ETHER	3	La magie est devenue une denrée rare sur Terre. Le Docteur Strange na plus que quelques artefacts pour protéger le monde des attaques mystiques, ses plus féroces adversaires en profitent donc pour l'attaquer. Affaibli, le Sorcier Suprême devra puiser dans toutes ses ressources.	136	2017-07-05 00:00:00	\N	1
161	DOCTOR STRANGE	RÉCIDIVE	4	Bien qu'il ait aidé le Dr Strange à vaincre l'Empirikul, Mister Misery est loin d'être son allié. Pendant des années, cet être magique a souffert à cause du Sorcier Suprême et il compte bien se venger en s'attaquant à Wong, le fidèle assistant du maître des arts mystiques.	128	2018-02-28 00:00:00	\N	1
162	DOCTOR STRANGE	SECRET EMPIRE	5	Sous le dôme de ténèbres qui recouvre New York, le Docteur Strange se doit de protéger les riverains des créatures surnaturelles qui rôdent. Pour y parvenir, il fait équipe avec des alliés inattendus, comme Spider-Woman et le Caïd.	128	2018-09-12 00:00:00	\N	1
173	IMPERIUM	UN MONDE MEILLEUR. À TOUT PRIX.	\N	Toyo Harada est l’homme le plus dangereux au monde. Psiotique doué de pouvoirs incommensurables, survivant d’Hiroshima et milliardaire philanthrope, il a passé sa vie à manipuler l’humanité dans l’ombre. Son but : réaliser sa vision du monde, à tout prix. Mais aujourd’hui, il est dos au mur. Ses pouvoirs ont été révélés au grand jour, son empire s’est effondré et il est pourchassé par les grandes puissances de la planète. Au lieu de se rendre, il va rassembler auprès de lui des êtres venus des plus sinistres recoins de l’univers et mettre en œuvre son plan. La guerre pour son utopie est déclarée !	440	2021-04-14 00:00:00	\N	3
\.


--
-- Data for Name: book_character; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.book_character (book_id, character_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	31
13	2
14	2
15	2
15	3
16	2
17	2
17	3
18	2
18	3
19	2
19	7
20	2
21	2
21	3
21	1
22	2
22	3
22	1
25	5
26	5
27	5
28	6
29	6
31	32
30	32
32	32
33	32
34	7
34	8
34	9
34	10
35	7
35	8
35	9
35	10
36	7
36	8
36	9
36	10
37	5
37	13
37	14
37	15
37	16
37	17
38	5
38	13
38	14
38	15
38	16
38	17
39	5
39	13
39	14
39	15
39	16
39	17
40	5
40	13
40	14
40	15
40	16
40	17
41	5
41	13
41	14
41	15
41	16
41	17
42	4
42	11
42	14
43	4
43	5
43	6
43	11
43	13
43	14
43	15
43	16
43	17
43	1
44	4
44	5
44	6
44	11
44	13
44	14
44	15
44	16
44	17
44	1
45	11
47	11
48	12
49	12
50	12
51	12
52	12
53	18
53	19
53	20
53	21
53	22
53	23
53	24
53	25
54	18
54	19
54	20
54	21
54	22
54	23
54	24
54	25
54	26
55	18
55	19
55	20
55	21
55	22
55	23
55	24
56	18
56	19
56	20
56	21
56	22
56	23
56	24
57	2
57	18
57	19
57	20
57	21
57	22
57	23
57	24
58	12
58	18
58	19
58	20
58	21
58	22
58	23
58	24
58	26
59	18
59	19
59	20
59	21
59	22
59	23
59	24
59	25
59	26
64	26
65	26
66	26
67	26
68	27
69	27
70	27
71	27
72	27
73	27
74	27
75	27
76	28
77	28
78	28
79	28
80	28
80	29
81	28
81	29
82	28
82	29
83	28
83	29
84	5
84	1
84	27
84	33
85	4
85	5
85	27
85	33
86	4
86	5
86	1
86	27
86	33
87	4
87	5
87	1
87	27
87	33
88	4
88	5
88	1
88	27
88	33
89	4
89	5
89	6
89	1
89	28
89	33
89	34
90	4
90	5
90	6
90	7
90	8
90	9
90	10
90	13
90	14
90	15
90	16
90	17
90	1
90	33
90	34
90	35
90	36
90	39
85	35
85	36
85	38
87	35
87	36
86	36
88	36
88	38
89	36
89	38
91	18
91	19
91	20
91	21
91	22
91	23
91	24
91	25
91	26
92	18
92	19
92	20
92	21
92	22
92	23
92	24
92	25
92	26
93	4
93	5
93	7
93	13
93	14
93	15
93	16
93	1
93	33
93	34
93	36
93	37
93	38
94	4
94	5
94	6
94	7
94	8
94	9
94	10
94	13
94	14
94	15
94	16
94	1
94	33
94	34
94	36
94	37
94	38
95	4
95	5
95	6
95	7
95	8
95	9
95	10
95	13
95	14
95	15
95	16
95	1
95	33
95	34
95	36
95	37
95	38
96	4
96	5
96	7
96	8
96	9
96	10
96	13
96	14
96	15
96	16
96	1
96	33
96	34
96	36
96	37
96	38
97	4
97	5
97	7
97	8
97	9
97	10
97	11
97	13
97	14
97	15
97	16
97	1
97	33
97	34
97	36
97	37
97	38
98	4
98	5
98	7
98	8
98	9
98	10
98	13
98	14
98	15
98	16
98	1
98	33
98	34
98	36
98	37
98	38
99	4
99	5
99	7
99	8
99	9
99	10
99	13
99	14
99	15
99	16
99	1
99	33
99	34
99	36
99	37
99	38
100	4
100	5
100	7
100	8
100	9
100	10
100	13
100	14
100	15
100	16
100	1
100	33
100	34
100	36
100	37
100	38
101	4
101	5
101	6
101	7
101	8
101	9
101	10
101	11
101	13
101	14
101	15
101	16
101	1
101	27
101	31
101	33
101	34
101	36
101	37
101	38
102	4
102	5
102	33
103	4
103	5
103	1
103	33
103	34
104	4
104	11
104	33
105	4
105	11
105	33
107	40
108	4
108	5
108	1
108	33
109	4
109	6
109	11
109	12
110	4
110	6
110	11
110	12
110	33
111	4
111	11
111	33
113	4
113	11
113	12
113	33
113	34
115	4
115	5
115	1
115	33
116	4
116	5
116	6
116	11
116	1
116	33
117	4
117	5
117	6
117	11
117	1
117	33
118	4
118	5
118	6
118	33
119	4
119	5
119	6
119	11
119	1
119	33
120	4
120	5
120	6
120	11
120	1
120	33
121	4
121	5
121	6
121	11
121	33
122	4
122	5
122	6
122	11
122	1
122	33
123	4
123	5
123	6
123	11
123	1
123	33
124	4
124	5
124	6
124	11
124	33
124	34
125	4
125	5
125	6
125	11
125	1
125	33
125	34
125	40
126	4
126	5
126	6
126	10
126	11
126	1
126	33
126	34
127	4
127	6
127	11
128	4
128	5
128	6
128	1
128	33
128	34
129	4
129	5
129	6
129	7
129	11
129	1
129	33
129	34
130	4
130	5
130	6
130	11
130	1
130	26
130	33
130	34
131	4
131	5
131	6
131	11
131	1
131	33
131	34
131	35
131	40
132	4
132	5
132	6
132	13
132	14
132	15
132	16
132	1
132	33
132	34
133	5
133	13
133	14
133	15
133	16
133	17
134	5
134	13
134	14
134	15
134	16
134	17
134	40
135	4
135	11
135	33
136	4
136	6
136	33
137	26
138	26
139	26
140	18
140	19
140	20
140	21
140	22
140	23
140	24
140	25
140	26
141	26
142	4
143	4
144	4
145	4
146	4
147	5
147	13
147	14
147	15
147	16
147	17
148	5
148	13
148	14
148	15
148	16
148	17
149	5
149	13
149	14
149	15
149	16
149	17
150	5
150	13
150	14
150	15
150	16
150	17
151	5
151	13
151	14
151	15
151	16
151	17
152	5
152	13
152	14
152	15
152	16
152	17
153	5
153	13
153	14
153	15
153	16
153	17
154	5
154	13
154	14
154	15
154	16
154	17
155	5
155	13
155	14
155	15
155	16
155	17
156	5
156	13
156	14
156	15
156	16
156	17
158	41
159	41
160	41
161	41
162	41
\.


--
-- Data for Name: book_format; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.book_format (book_id, format_id) FROM stdin;
1	5
2	5
3	5
4	5
5	5
6	5
7	5
8	3
9	3
10	1
11	1
12	1
14	3
15	3
16	3
21	2
17	3
13	3
18	3
19	3
20	3
22	4
23	1
24	1
25	1
26	4
27	1
28	1
29	1
30	1
31	2
32	2
33	2
34	5
35	5
36	5
38	6
39	6
40	5
42	3
43	3
44	3
45	3
46	3
47	3
48	1
49	1
50	1
51	3
52	3
53	3
54	3
55	7
56	7
57	7
58	7
59	7
60	2
61	2
62	2
63	3
64	3
65	3
66	7
67	7
68	3
69	3
70	3
71	3
73	1
74	1
75	1
76	1
77	1
78	8
79	8
80	9
81	9
82	9
83	9
84	1
85	3
86	3
87	3
88	3
89	5
90	5
91	5
92	5
93	3
94	3
95	3
96	3
97	3
98	3
99	3
100	3
101	1
102	3
103	3
104	1
105	1
106	3
107	3
108	3
109	3
110	3
111	1
113	3
115	1
116	9
117	9
118	7
119	7
120	7
121	7
122	7
123	7
124	3
125	3
126	3
127	3
128	3
129	3
130	3
131	3
132	1
133	3
134	3
135	1
136	1
137	7
138	7
139	7
140	3
141	7
142	3
143	3
144	3
145	3
146	3
147	2
148	2
149	2
150	2
151	3
152	3
153	3
154	3
155	3
156	3
157	8
158	2
159	2
160	2
161	2
162	2
\.


--
-- Data for Name: character; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."character" (id, first_name, last_name, alias, side, nature_id) FROM stdin;
2	\N	\N	Venom	f	\N
3	\N	\N	Carnage	f	\N
4	Tony	Stark	Iron-Man	t	\N
5	James	Logan	Wolverine	t	\N
6	Bruce	Banner	Hulk	t	\N
7	Reed	Richards	Mister Fantastic	t	\N
8	Sue	Storm	La femme invisible	t	\N
9	Johnny	Storm	La torche humaine	t	\N
10	Benjamin	Grimm	La chose	t	\N
11	Thor	Odinson	Thor	t	\N
12	\N	\N	Thanos	f	\N
13	Scott	Summers	Cyclope	t	\N
14	Ororo	Munroe	Tornade	t	\N
15	Charles	Xavier	Professeur Xavier	t	\N
16	Hank	McCoy	Le fauve	t	\N
17	Max	Eisenhard	Magneto	t	\N
18	Peter	Quill	Star-Lord	t	\N
19	Rocket	\N	Rocket Racoon	t	\N
20	Drax	\N	Drax le Destructeur	t	\N
21	Groot	\N	Groot	t	\N
22	Gamora	\N	Gamora	t	\N
23	Mantis	\N	Mantis	t	\N
24	Nébula	\N	Nébula	t	\N
25	Adam	Warlock	Adam Warlock	t	\N
26	Richard	Rider	Nova	t	\N
27	Matthew	Murdock	Daredevil	t	\N
28	Wade	Wilson	Deadpool	t	\N
29	Nathan	Summers	Cable	t	\N
31	Miles	Morales	Miles Morales	t	\N
32	\N	\N	Maestro	f	\N
33	Steve	Rogers	Captain America	t	\N
34	T'Chala	\N	Black Panther	t	\N
35	Wanda	Maximoff	La sorcière rouge	t	\N
36	Luke	Cage	Power Man	t	\N
37	Natasha	Romanof	Black Widow	t	\N
38	Clint	Barton	Hawkeye	t	\N
39	Pietro	Maximoff	Quicksilver	t	\N
40	Boltagon	Blackagar	Black Bolt	t	\N
1	Peter	Parker	Spider-Man	t	\N
41	Stephen	Strange	Doctor Strange	t	\N
\.


--
-- Data for Name: cover; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cover (id, name, image_size, mime_type, updated_at) FROM stdin;
1	6640ff83e1f81_ultimate-spider-man-tome-1-front.jpg	477006	image/png	2024-05-12 17:42:27
2	66410c31cd323_ultimate-spider-man-tome-2-front.jpg	463243	image/png	2024-05-12 18:36:33
3	66411905f1c9c_ultimate-spider-man-tome-3-front.jpg	467132	image/png	2024-05-12 19:31:17
4	66411a2061510_ultimate-spider-man-compendium-front.jpg	431662	image/png	2024-05-12 19:36:00
9	66478e62cebc9_absolute-carnage-front.jpeg	93948	image/jpeg	2024-05-17 17:05:38
10	664794b75b019_iron-man-les-cinq-cauchemars-front.jpg	554288	image/png	2024-05-17 17:32:39
11	664795868c052_iron-man-extremis-front.jpg	438014	image/png	2024-05-17 17:36:06
13	6647a6ecb540f_miles-morales-ultimate-spider-man-omnibus-front.jpg	471714	image/png	2024-05-17 18:50:20
14	6647a774d47a6_spider-man-miles-morales-volume-2-front.jpg	527540	image/png	2024-05-17 18:52:36
15	6647aa0b454e8_ultimate-spider-man-la-mort-de-spider-man-front.jpg	459086	image/jpeg	2024-05-17 19:03:39
16	6647aabd87289_spider-man-l-autre-front.jpg	485680	image/png	2024-05-17 19:06:37
17	6647ab266ecc5_spider-man-retour-au-noir-front.jpg	573849	image/png	2024-05-17 19:08:22
18	6647ab5db48e3_spider-man-vocation-front.jpg	481899	image/png	2024-05-17 19:09:17
19	6647ab868d09a_spider-man-spiderverse-front.jpg	437717	image/png	2024-05-17 19:09:58
20	6647ac951be99_venom-tome-1-rex-front.jpg	465027	image/png	2024-05-17 19:14:29
21	6647aca936bae_venom-tome-2-abysse-front.jpg	473541	image/png	2024-05-17 19:14:49
22	6647acb8df599_venom-tome-3-dechaine-front.jpg	534827	image/png	2024-05-17 19:15:04
23	6647acf3c4fd6_venom-tome-4-la-guerre-des-royaumes-front.jpg	464490	image/png	2024-05-17 19:16:03
24	6647ad19b0d05_venom-tome-5-absolute-carnage-front.jpg	505124	image/png	2024-05-17 19:16:41
25	6647ad3cb0467_venom-tome-6-venom-island-front.jpg	413476	image/png	2024-05-17 19:17:16
26	6647ad4eeefaa_venom-tome-7-ailleurs-front.jpg	504200	image/png	2024-05-17 19:17:34
27	6647ade3b9f5b_venom-tome-8-front.jpg	509169	image/png	2024-05-17 19:20:03
28	6647ae75ef51d_king-in-black-front.jpeg	35379	image/jpeg	2024-05-17 19:22:29
29	6647af1540d1b_spider-man-le-dernier-combat-front.jpg	505070	image/png	2024-05-17 19:25:09
30	6647bf815b5e7_wolverine-old-man-logan-front.jpg	472735	image/png	2024-05-17 20:35:13
31	6647c010490dd_wolverine-la-mort-de-wolverine-front.jpg	393987	image/png	2024-05-17 20:37:36
32	6647c0b0a2b9b_wolverine-ennemi-d-etat-front.jpg	440821	image/png	2024-05-17 20:40:16
33	6647c1bf38f6e_hulk-planete-hulk-front.jpg	466876	image/png	2024-05-17 20:44:47
34	6647c1cb0436e_hulk-world-war-hulk-front.jpg	518444	image/png	2024-05-17 20:44:59
35	6647c2419c516_hulk-futur-imparfait-front.jpg	495234	image/png	2024-05-17 20:46:57
36	6647c33088617_maestro-world-war-m-front.jpg	537883	image/png	2024-05-17 20:50:56
37	6647c3ad72638_maestro-futur-imparfait-front.jpg	495234	image/png	2024-05-17 20:53:01
39	6647c539d6d79_maestro-symphonie-en-gamma-majeur-front.jpg	524402	image/png	2024-05-17 20:59:37
40	6647c7806159e_fantastic-four-par-waid-wieringo-front.jpg	526466	image/png	2024-05-17 21:09:20
41	6647c78cd7d2c_fantastic-four-par-hickman-tome-1-front.jpg	520206	image/png	2024-05-17 21:09:32
42	6647c7962d8ed_fantastic-four-par-hickman-tome-2-front.jpg	442039	image/png	2024-05-17 21:09:42
43	6647c9e193ea2_new-x-men-e-comme-extinction-front.jpg	498179	image/png	2024-05-17 21:19:29
44	6647ca0f2bb42_new-x-men-planète-x-front.jpg	539758	image/png	2024-05-17 21:20:15
46	6647cb7386f1c_x-men-le-retour-du-messie-front.jpg	550158	image/png	2024-05-17 21:26:11
47	6647cd063551d_maestro-guerre-et-pax-front.jpg	510528	image/png	2024-05-17 21:32:54
48	6647cd8a43b95_xmen-schism-front.jpg	492982	image/png	2024-05-17 21:35:06
49	6648ddeb8786a_x-men-le-complexe-du-messie.jpg	492920	image/jpeg	2024-05-18 16:57:15
50	6648de5e47abe_x-men-axis.jpg	143362	image/jpeg	2024-05-18 16:59:10
51	6648df8e21aec_avengers-vs-xmen-tome-1.jpg	447939	image/png	2024-05-18 17:04:14
52	6648df9a7345c_avengers-vs-xmen-tome-2.jpg	422022	image/png	2024-05-18 17:04:26
53	6648e5272e488_thor-tome-1-renaissance.jpg	467483	image/png	2024-05-18 17:28:07
54	6648e53526feb_thor-tome-2-victoire.jpg	476366	image/png	2024-05-18 17:28:21
55	6648e5f5745a0_thor-war-of-the-realms.jpg	551639	image/png	2024-05-18 17:31:33
56	6648e6d7318a6_thanos-gagne.jpg	61754	image/webp	2024-05-18 17:35:19
57	6648e7279fee1_thanos-gagne.jpg	477279	image/png	2024-05-18 17:36:39
58	6648e7a310ed5_thanos-le-retour-de-thanos.jpg	493588	image/png	2024-05-18 17:38:43
59	6648e846beffe_thanos-imperative.jpg	484794	image/png	2024-05-18 17:41:26
60	6648e8aed55c8_thanos-l-ascension-de-thanos.jpg	493131	image/png	2024-05-18 17:43:10
61	6648e92e8bf9c_thanos-le-samaritain.jpg	507857	image/png	2024-05-18 17:45:18
62	6648ec96bca33_les-gardiens-de-la-galaxie-tome-1-heritage.jpg	262154	image/jpeg	2024-05-18 17:59:50
63	6648eca06d221_les-gardiens-de-la-galaxie-tome-2-war-of-kings.jpg	121718	image/jpeg	2024-05-18 18:00:00
64	6648ee3fc4e3f_les-gardiens-de-la-galaxie-tome-2-angela.jpg	318720	image/jpeg	2024-05-18 18:06:55
65	6648ee511a847_les-gardiens-de-la-galaxie-tome-1-cosmic-avengers.jpg	233201	image/jpeg	2024-05-18 18:07:13
66	6648ef55a677b_les-gardiens-de-la-galaxie-tome-3-la-fin-des-gardiens.jpg	449961	image/jpeg	2024-05-18 18:11:33
67	6648f00d42321_les-gardiens-de-la-galaxie-tome-4-original-sin.jpg	363427	image/jpeg	2024-05-18 18:14:37
68	6648f0becd5bc_les-gardiens-de-la-galaxie-tome-5-les-gardiens-rencontrent-les-avengers.jpg	384104	image/jpeg	2024-05-18 18:17:34
69	6648f1f646ce7_les-eternels-tome-1-seule-la-mort-est-eternelle.jpg	462187	image/png	2024-05-18 18:22:46
70	6648f201a1104_les-eternels-tome-2-gloire-a-thanos.jpg	468275	image/png	2024-05-18 18:22:57
71	6648f20dd3de8_les-eternels-tome-3-une-histoire-ecrite-dans-le-sang.jpg	463750	image/png	2024-05-18 18:23:09
72	6648f25649eb7_les-eternels-braver-l-apocalypse.jpg	489643	image/png	2024-05-18 18:24:22
73	6648f48f814f0_nova-tome-1-annihilation-conquest.jpg	216315	image/jpeg	2024-05-18 18:33:51
74	6648f498eca72_nova-tome-2-secret-invasion.jpg	261796	image/jpeg	2024-05-18 18:34:00
75	6648f5d71ebba_nova-tome-1-origines.jpg	84485	image/jpeg	2024-05-18 18:39:19
76	6648f61215f54_nova-tome-2-le-rookie.jpg	230678	image/jpeg	2024-05-18 18:40:18
77	6648f77eb9a73_daredevil-tome-1-underboss.jpg	443251	image/png	2024-05-18 18:46:22
78	6648f78adbcdb_daredevil-tome-2-le-petit-maitre.jpg	546869	image/png	2024-05-18 18:46:34
79	6648f794a2555_daredevil-tome-3-roi-de-hells-kitchen.jpg	533596	image/png	2024-05-18 18:46:44
80	6648f79e03325_daredevil-tome-4-le-rapport-murdock.jpg	495915	image/png	2024-05-18 18:46:54
81	6648f8a560501_daredevil-renaissance.jpg	432609	image/png	2024-05-18 18:51:17
82	6648f8b90b0e2_dardevil-jaune.jpg	393190	image/png	2024-05-18 18:51:37
84	6648f90bbfb5b_daredevil-sous-l-aile-du-diable.jpg	497169	image/png	2024-05-18 18:52:59
85	6648f94e4081c_daredevil-l-homme-sans-peur.jpg	189321	image/png	2024-05-18 18:54:06
86	6648fa4c520c5_deadpool-il-faut-soigner-le-soldat-wilson.jpg	527047	image/png	2024-05-18 18:58:20
87	6648fa917b812_deadpool-massacre-marvel.jpg	434397	image/png	2024-05-18 18:59:29
88	6648fb37047b3_deadpool-re-massacre-marvel.jpg	228598	image/jpeg	2024-05-18 19:02:15
89	6648fb92e1c48_deadpool-massacre-deadpool.jpg	458118	image/png	2024-05-18 19:03:46
90	66490047af0a1_deadpool-&-cable-tome-1-le-culte-de-la-personnalite.jpg	231777	image/jpeg	2024-05-18 19:23:51
91	6649005bdb1eb_deadpool-&-cable-tome-2-legendes-vivantes.jpg	253894	image/jpeg	2024-05-18 19:24:11
92	6649014dc2f50_deadpool-&-cable-tome-3-l-effet-domino.jpg	169782	image/jpeg	2024-05-18 19:28:13
93	664901d723b4e_deadpool-&-cable-tome-4-deux-mutants-et-un-couffin.jpg	231502	image/jpeg	2024-05-18 19:30:31
94	6649031e170ed_avengers-secret-war.jpg	480873	image/png	2024-05-18 19:35:58
96	6649ce9d7bb53_new-avengers-tome-1-chaos.jpg	264950	image/jpeg	2024-05-19 10:04:13
97	6649ceac44a13_new-avengers-tome-2-secrets-et-mensonges.jpg	216995	image/jpeg	2024-05-19 10:04:28
98	6649cfb544ce7_the-new-avengers-tome-3-revolution.jpg	257659	image/jpeg	2024-05-19 10:08:53
99	6649d072c1355_the-new-avengers-tome-4-confiance.jpg	213386	image/jpeg	2024-05-19 10:12:02
100	6649d14a59856_the-new-avengers-tome-5-l-empire.jpg	87683	image/jpeg	2024-05-19 10:15:38
103	6649d26f67c0d_house-of-m.jpg	414188	image/png	2024-05-19 10:20:31
104	6649d4eada6da_avengers-annihilation.jpg	461735	image/png	2024-05-19 10:31:06
105	6649d5b51fc9e_avengers-annihilation-conquest.jpg	432279	image/png	2024-05-19 10:34:29
107	664b92b065fbd_civil-war-prelude.jpg	208097	image/png	2024-05-20 18:13:04
108	664b92f754f0f_civil-war-volume-1.jpg	283438	image/png	2024-05-20 18:14:15
109	664b9305a2ecf_civil-war-volume-2.jpg	271809	image/png	2024-05-20 18:14:29
110	664b93ca2d10d_civil-war-volume-4.jpg	303603	image/png	2024-05-20 18:17:46
111	664b942a740e8_civil-war-volume-5.jpg	216842	image/png	2024-05-20 18:19:22
112	664b945719561_civil-war-volume-3.jpg	281195	image/png	2024-05-20 18:20:07
113	664b94cf0d980_civil-war-volume-6.jpg	285308	image/png	2024-05-20 18:22:07
114	664b95d793c87_civil-war-2.jpg	530019	image/jpeg	2024-05-20 18:26:31
115	664b966097950_secret-wars.jpg	507152	image/png	2024-05-20 18:28:48
116	664b96e1127f2_dark-avengers-rassemblement.jpg	348611	image/png	2024-05-20 18:30:57
117	664ba83919f7c_dark-avengers-tome-2-exodus.jpg	416074	image/png	2024-05-20 19:44:57
118	664bb2aab02fe_siege.jpg	426818	image/png	2024-05-20 20:29:30
119	664bb2e3439ff_secret-invasion.jpg	455984	image/png	2024-05-20 20:30:27
120	664bb35a8bd64_war-of-kings.jpg	279584	image/jpeg	2024-05-20 20:32:26
121	664bbc20eac7e_realm-of-kings.jpg	195877	image/jpeg	2024-05-20 21:09:52
122	664bbcf0ef4a4_avengers-l-age-des-heros.jpg	296756	image/jpeg	2024-05-20 21:13:20
123	664bbe41096d5_avengers-time-runs-out-tome-1-tu-ne-peux-pas-gagner.jpg	244332	image/jpeg	2024-05-20 21:18:57
124	664bbe4a699ad_avengers-time-runs-out-tome-2-la-chute-des-dieux.jpg	274884	image/jpeg	2024-05-20 21:19:06
126	664bbf21da572_avengers-fear-itself.jpg	481821	image/png	2024-05-20 21:22:41
127	664bc690951d8_avengers-infinity-tome-1.jpg	498499	image/png	2024-05-20 21:54:24
128	664bc716efebe_avengers-infinity-tome-2.jpg	241799	image/jpeg	2024-05-20 21:56:38
129	664bc7996ea13_avengers-age-of-ultron.jpg	530192	image/png	2024-05-20 21:58:49
130	664cf60e897a1_fear-itself-tome-1.jpg	97716	image/jpeg	2024-05-21 19:29:18
131	664cf61891684_fear-itself-tome-2.jpg	104605	image/jpeg	2024-05-21 19:29:28
132	664cfa06835a5_avengers-tome-1-le-monde-des-avengers.jpg	538385	image/png	2024-05-21 19:46:14
133	664cfa1b95521_avengers-tome-2-le-dernier-instant-blanc.jpg	205602	image/jpeg	2024-05-21 19:46:35
134	664cfa635f6a8_avengers-tome-3-prelude-a-infinity.jpg	273674	image/jpeg	2024-05-21 19:47:47
135	664cfa8c60ba7_avengers-tome-4-infinity.jpg	189436	image/jpeg	2024-05-21 19:48:28
136	664cfa9cc9557_avengers-tome-5-planete-vagabonde.jpg	295068	image/jpeg	2024-05-21 19:48:44
137	664cfaaaa798e_avengers-tome-6-le-dernier-avenger.jpg	259245	image/jpeg	2024-05-21 19:48:58
138	664cfbb16b44e_new-avengers-tome-1-tout-meurt.jpg	443201	image/png	2024-05-21 19:53:21
139	664cfbc17637a_new-avengers-tome-2-un-monde-parfait.jpg	539076	image/png	2024-05-21 19:53:37
140	664cfceb607ef_avengers-original-sin.jpg	390320	image/png	2024-05-21 19:58:35
141	664cfd3b01a0d_avengers-original-sin-hulk-iron-man-thor.jpg	441118	image/png	2024-05-21 19:59:55
142	664cfd65496cd_avengers-secret-empire.jpg	549025	image/png	2024-05-21 20:00:37
143	664d00feef260_avengers-what-if-tome-1.jpg	288463	image/png	2024-05-21 20:15:58
144	664d010ac5f56_avengers-what-if-tome-2.jpg	516771	image/png	2024-05-21 20:16:10
145	664d0115b85c6_avengers-what-if-tome-3.jpg	442367	image/png	2024-05-21 20:16:21
146	664d01f7e6e37_avengers-forever.jpg	562135	image/png	2024-05-21 20:20:07
147	664d02aa53f01_x-men-death-of-x.jpg	448170	image/png	2024-05-21 20:23:06
148	664d02b78a3b9_x-men-inhumans-vs-x-men.jpg	498748	image/png	2024-05-21 20:23:19
150	664d0d5c9f800_avengers-reunion.jpg	388522	image/png	2024-05-21 21:08:44
151	664d0e14c3605_avengers-zone-rouge.jpg	108864	image/jpeg	2024-05-21 21:11:48
152	66519e0ec464d_nova-tome-3-nova-corpse.jpg	446299	image/png	2024-05-25 08:15:10
153	6651a06b299eb_nova-tome-4-la-verite-sur-les-black-nova.jpg	462659	image/png	2024-05-25 08:25:15
154	6651a096435ce_nova-tome-5-carte-de-membre.jpg	563204	image/jpeg	2024-05-25 08:25:58
155	6651a17cd81ae_les-gardiens-de-la-galaxie-tome-3-realm-of-kings.jpg	237822	image/jpeg	2024-05-25 08:29:48
156	66521c9bb08c2_nova-tome-6-retrouvailles.jpg	26747	image/jpeg	2024-05-25 17:15:07
157	6653a597e504d_the-invincible-iron-man-tome-01-dans-la-ligne-de-mire.jpg	323560	image/jpeg	2024-05-26 21:11:51
158	6653a61a3f04f_the-invincible-iron-man-tome-02-dislocation.jpg	297497	image/jpeg	2024-05-26 21:14:02
159	6653a81ebff39_the-invincible-iron-man-tome-05-demon.jpg	213688	image/jpeg	2024-05-26 21:22:38
160	66579aba76572_the-invincible-iron-man-tome-04-fear-itself.jpg	232090	image/jpeg	2024-05-29 21:14:34
161	66579bb1a9320_the-invincible-iron-man-tome-03-stark-resistance.jpg	202596	image/jpeg	2024-05-29 21:18:41
162	6660e3bf8471f_x-men-x-of-swords-01.jpg	513048	image/png	2024-06-05 22:16:31
163	6660e3cfe2e8e_x-men-x-of-swords-02.jpg	386902	image/png	2024-06-05 22:16:47
164	6660e3dd27751_x-men-x-of-swords-03.jpg	497745	image/png	2024-06-05 22:17:01
165	6660e3e912ddd_x-men-x-of-swords-04.jpg	512865	image/png	2024-06-05 22:17:13
166	6660e4c3d357b_x-men-house-of-x-powers-of-x.jpg	447364	image/png	2024-06-05 22:20:51
167	666ddb5ab4b14_x-men-volume-1-pax-krakoa.jpg	476231	image/png	2024-06-15 18:20:10
168	666ddb65b523b_x-men-volume-2-le-commencement.jpg	326997	image/png	2024-06-15 18:20:21
169	666ddbc3ac6ca_x-men-inferno.jpg	500897	image/png	2024-06-15 18:21:55
170	666ddc2f013bd_x-men-hellfire-gala.jpg	456587	image/png	2024-06-15 18:23:43
171	666ddc72c92f3_x-men-giant-size.jpg	503099	image/png	2024-06-15 18:24:50
172	666ddd03ab360_x-men-volume-2-le-commencement.jpg	450272	image/png	2024-06-15 18:27:15
173	6670956a80fb0_doctor-strange-le-debut-et-la-fin.jpg	211808	image/jpeg	2024-06-17 19:58:34
174	6670957ec80ee_doctor-strange-tome-1-les-voies-de-l-etrange.jpg	262031	image/jpeg	2024-06-17 19:58:54
175	6670958cd7444_doctor-strange-tome-2-le-crepuscule-de-la-magie.jpg	185767	image/jpeg	2024-06-17 19:59:08
176	6670959fea753_doctor-strange-tome-3-du-sang-dans-l-ether.jpg	167631	image/jpeg	2024-06-17 19:59:27
177	667095b050911_doctor-strange-tome-4-recidive.jpg	226607	image/jpeg	2024-06-17 19:59:44
178	667095be557d1_doctor-strange-tome-5-secret-empire.jpg	335849	image/png	2024-06-17 19:59:58
179	667888bba7745_doctor-strange-tome-2-le-crepuscule-de-la-magie.jpg	185767	image/jpeg	2024-06-23 20:42:35
180	66788b62c040f_x-o-manowar-tome-01.jpg	539918	image/webp	2024-06-23 20:53:54
181	66788b7c8bfed_x-o-manowar-tome-02.jpg	532931	image/jpeg	2024-06-23 20:54:20
182	66788b840c94e_x-o-manowar-tome-03.jpg	326352	image/jpeg	2024-06-23 20:54:28
183	66788c57f181e_the-valiant.jpg	708764	image/webp	2024-06-23 20:57:59
184	66788d1643c52_ninjak.jpg	84200	image/webp	2024-06-23 21:01:10
185	66788d9714ee0_bloodshot.jpg	997790	image/webp	2024-06-23 21:03:19
186	66788ebaf3915_archer-and-armstrong-a+a.jpg	139608	image/webp	2024-06-23 21:08:11
187	66788ec852a6e_archer-and-armstrong-integrale.jpg	406406	image/webp	2024-06-23 21:08:24
188	66788fad27458_faith.jpg	534350	image/webp	2024-06-23 21:12:13
189	6678907a1a818_harbinger.jpg	680898	image/webp	2024-06-23 21:15:38
190	6678915c0fc51_imperium.jpg	118606	image/webp	2024-06-23 21:19:24
191	667891f2d2a95_vie-et-mort-de-toyo-harada.jpg	312642	image/jpeg	2024-06-23 21:21:54
192	667892c4a5f4b_divinity.jpg	385890	image/jpeg	2024-06-23 21:25:24
193	667893017593b_unity.jpg	609630	image/webp	2024-06-23 21:26:25
194	667893f6f2350_quantum-and-woody-must-die.jpg	136235	image/jpeg	2024-06-23 21:30:30
195	6678946db45ea_the-delinquents.jpg	471899	image/jpeg	2024-06-23 21:32:29
196	667895bf96df4_rai-integrale-tome-1.jpg	583864	image/webp	2024-06-23 21:38:07
197	667896075cc76_rai-integrale-tome-2.jpg	131770	image/jpeg	2024-06-23 21:39:19
198	6678974e17a4a_eternal-warrior.jpg	435390	image/webp	2024-06-23 21:44:46
199	66789760b484e_ivar-timewalker.jpg	389242	image/webp	2024-06-23 21:45:04
\.


--
-- Data for Name: cover_book; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cover_book (cover_id, book_id) FROM stdin;
13	6
14	7
1	1
2	2
3	3
4	5
9	21
10	23
11	24
15	4
16	8
17	9
18	11
19	12
20	13
21	14
22	15
23	16
24	17
25	18
26	19
27	20
28	22
29	10
30	25
31	26
32	27
33	28
34	29
36	31
37	30
39	32
40	34
41	35
42	36
43	37
44	38
46	40
47	33
48	41
49	39
50	42
51	43
52	44
53	45
54	46
55	47
57	48
58	49
59	51
60	50
61	52
62	53
63	54
64	56
65	55
66	57
67	58
68	59
69	60
70	61
71	62
72	63
73	64
74	65
75	66
76	67
77	68
78	69
79	70
80	71
81	74
82	72
84	73
85	75
86	76
87	77
88	78
89	79
90	80
91	81
92	82
93	83
94	84
96	85
97	86
98	87
99	88
100	89
103	90
104	91
105	92
107	93
108	94
109	95
110	97
111	98
112	96
113	99
114	100
115	101
116	102
117	103
118	104
119	105
120	106
121	107
122	108
123	109
124	110
126	111
127	112
128	113
129	115
130	116
131	117
132	118
133	119
134	120
135	121
136	122
137	123
138	124
139	125
140	126
141	127
142	128
143	129
144	130
145	131
146	132
147	133
148	134
150	135
151	136
152	137
153	138
154	139
155	140
156	141
157	142
158	143
159	144
160	145
161	146
162	147
163	148
164	149
165	150
166	151
167	152
169	154
170	155
171	156
172	153
173	157
174	158
175	159
176	160
177	161
178	162
180	163
181	164
182	165
183	166
184	167
185	168
186	170
187	169
188	171
189	172
190	173
191	174
192	175
193	176
194	177
195	178
196	179
197	180
198	181
199	182
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20240511131732	2024-05-11 13:17:55	73
DoctrineMigrations\\Version20240511132606	2024-05-11 13:26:11	11
DoctrineMigrations\\Version20240511140525	2024-05-11 14:05:44	14
DoctrineMigrations\\Version20240511140629	2024-05-11 14:06:37	5
DoctrineMigrations\\Version20240511143034	2024-05-11 14:30:39	78
DoctrineMigrations\\Version20240511225237	2024-05-11 22:52:50	70
DoctrineMigrations\\Version20240512004012	2024-05-12 00:40:20	55
DoctrineMigrations\\Version20240512163749	2024-05-12 16:37:56	94
DoctrineMigrations\\Version20240512173341	2024-05-12 17:33:46	27
DoctrineMigrations\\Version20240512184019	2024-05-12 18:40:27	4
DoctrineMigrations\\Version20240517184037	2024-05-17 18:40:50	72
DoctrineMigrations\\Version20240517184353	2024-05-17 18:43:59	5
DoctrineMigrations\\Version20240519191921	2024-05-19 19:19:39	75
DoctrineMigrations\\Version20240519192905	2024-05-19 19:31:06	4
DoctrineMigrations\\Version20240623190740	2024-06-23 19:23:13	35
\.


--
-- Data for Name: event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event (id, title) FROM stdin;
\.


--
-- Data for Name: format; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.format (id, entitled, description) FROM stdin;
1	Marvel Must Have	Retrouvez des récits complets, incontournables et accessibles.
2	100% Marvel	Découvrez les récits incontournables de vos héros Marvel préférés chez Panini Comics comme Immortal Hulk, le Daredevil de Chip Zdarsky ou encore le Venom de Donny Cates, tous chez Panini Comics. Super-héros urbains ou pouvoirs mystiques, il y en a pour tous les goûts !
4	Marvel Absolute	Leur taille géante embellie par un fourreau, feront trôner fièrement ces albums sur les étagères. Spider-Man, les X-Men ou Captain America, tous les plus grands héros ont eu les honneurs de voir leurs aventures publiées de cette collection de prestige.
3	Marvel Deluxe	Les meilleurs récits de vos super-héros préférés Marvel en français dans un grand format. Une édition prestigieuse pour profiter au mieux des aventures de Spider-Man en comics et autres grands évènements incontournables tels que Civil War.
5	Marvel Omnibus	Un incontournable pour apprécier les grandes épopées comme Onslaught, Ultimate Spider-Man, La saga du Clone et bien plus encore. Le tout dans des pavés à la pagination conséquente pour avoir des belles heures de lecture et aussi, en même temps, pour faire de beaux bras musclés !
6	Marvel Icons	Une collection de "gros pavés" qui vous permettront de découvrir les arcs narratifs les plus passionnants et les plus haletants sur les plus grands héros de Marvel !
7	Marvel Now	es aventures inédites dans des formats cartonnés. Laissez-vous séduire par Nova, par Les Gardiens de la Galaxie ou par Iron Man (pour les plus connus)... Le renouveau de Marvel est à la porté de vos mains.
8	Marvel Dark	Serez-vous assez téméraire pour collectionner les albums Marvel Dark ?
9	Marvel Monster	\N
\.


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.job (id, title, description) FROM stdin;
\.


--
-- Data for Name: job_author; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.job_author (job_id, author_id) FROM stdin;
\.


--
-- Data for Name: messenger_messages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.messenger_messages (id, body, headers, queue_name, created_at, available_at, delivered_at) FROM stdin;
\.


--
-- Data for Name: nature; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nature (id, name, description) FROM stdin;
1	Humain	\N
2	Mutant	\N
3	Asgardien	\N
4	Kree	\N
5	Skrull	\N
6	Inhumain	\N
7	Éternel	\N
8	Céleste	\N
\.


--
-- Data for Name: portrait; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.portrait (id, name, image_size, mime_type, updated_at) FROM stdin;
1	664b20461de69_venom.jpg	102536	image/jpeg	2024-05-20 10:04:54
2	664b20b2d9673_carnage.jpg	185151	image/jpeg	2024-05-20 10:06:42
3	664b2286c3e8d_iron-man.jpg	240738	image/webp	2024-05-20 10:14:30
4	664b229372c33_wolverine.jpg	96239	image/jpeg	2024-05-20 10:14:43
5	664b40877a6f5_hulk.jpg	524173	image/jpeg	2024-05-20 12:22:31
6	664b409542842_mister-fantastic.jpg	83996	image/webp	2024-05-20 12:22:45
7	664b409eb46ea_suzan-storm.jpg	33745	image/jpeg	2024-05-20 12:22:54
8	664b40ab36bf0_la-torche-humaine.jpg	82773	image/jpeg	2024-05-20 12:23:07
9	664b40ba436c2_la-chose.jpg	295285	image/jpeg	2024-05-20 12:23:22
10	664b451f9c1db_thor.jpg	169263	image/jpeg	2024-05-20 12:42:07
11	664b459d0d9f1_thanos.jpg	112814	image/jpeg	2024-05-20 12:44:13
12	664b8633c132b_cyclope.jpg	145330	image/webp	2024-05-20 17:19:47
13	664b8675ca05e_tornade.jpg	108864	image/webp	2024-05-20 17:20:53
14	664b86b73d342_professeur-x.jpg	58252	image/jpeg	2024-05-20 17:21:59
15	664b86f93e3a8_le-fauve.jpg	168373	image/jpeg	2024-05-20 17:23:05
16	664b875700cf0_magneto.jpg	27841	image/jpeg	2024-05-20 17:24:39
17	664b876e87ac0_spider-man.jpg	305346	image/jpeg	2024-05-20 17:25:02
18	664b88108f08c_star-lord.jpg	27404	image/webp	2024-05-20 17:27:44
19	664b88e971951_rocket-racoon.jpg	92621	image/jpeg	2024-05-20 17:31:21
20	664b89426192c_drax.jpg	116180	image/jpeg	2024-05-20 17:32:50
21	664b8984e4bd4_groot.jpg	341555	image/jpeg	2024-05-20 17:33:56
22	664b89cab6aac_gamora.jpg	146898	image/webp	2024-05-20 17:35:06
23	664b89fbd0f80_mantis.jpg	247680	image/webp	2024-05-20 17:35:55
24	664b8aed93a0c_nebula.jpg	137772	image/webp	2024-05-20 17:39:57
25	664b8b4aac32b_adam-warlock.jpg	272074	image/webp	2024-05-20 17:41:30
26	664b8b7ed354c_nova.jpg	33142	image/webp	2024-05-20 17:42:22
27	664b8bd0846b9_daredevil.jpg	256006	image/webp	2024-05-20 17:43:44
29	664b8d308a24f_deadpool.jpg	150608	image/jpeg	2024-05-20 17:49:36
30	664b8d6b3b4cf_cable.jpg	309336	image/webp	2024-05-20 17:50:35
31	664b8dc12ed48_miles-morales.jpg	33572	image/webp	2024-05-20 17:52:01
32	664b8e4e3bfdc_maestro.jpg	191620	image/webp	2024-05-20 17:54:22
33	664b8ec52a35b_captain-america.jpg	126088	image/jpeg	2024-05-20 17:56:21
34	664b8f1ba6ebe_black-panther.jpg	1719784	image/jpeg	2024-05-20 17:57:47
35	664b8f51c2b4a_la-sorciere-rouge.jpg	171052	image/webp	2024-05-20 17:58:41
36	664b8f7e1e6a5_power-man.jpg	290146	image/webp	2024-05-20 17:59:26
37	664b8fca03103_black-widow.jpg	363441	image/jpeg	2024-05-20 18:00:42
38	664b90510c19a_hawkeye.jpg	100675	image/jpeg	2024-05-20 18:02:57
39	664b90bb35155_quicksilver.jpg	159135	image/jpeg	2024-05-20 18:04:43
40	664bae16b323f_black-bolt.jpg	29063	image/jpeg	2024-05-20 20:09:58
41	667093e4f19e9_doctor-strange.jpeg	109004	image/jpeg	2024-06-17 19:52:04
\.


--
-- Data for Name: portrait_character; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.portrait_character (portrait_id, character_id) FROM stdin;
1	2
2	3
3	4
4	5
5	6
6	7
7	8
8	9
9	10
10	11
11	12
12	13
13	14
14	15
15	16
16	17
17	1
18	18
19	19
20	20
21	21
22	22
23	23
24	24
25	25
26	26
27	27
29	28
30	29
31	31
32	32
33	33
34	34
35	35
36	36
37	37
38	38
39	39
40	40
41	41
\.


--
-- Data for Name: team; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.team (id, name, description) FROM stdin;
1	Les 4 fantastiques	\N
2	Les X-Men	\N
3	Les Gardiens de la galaxie	\N
4	Les Avengers	\N
\.


--
-- Data for Name: team_character; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.team_character (team_id, character_id) FROM stdin;
1	7
1	8
1	9
1	10
2	5
2	13
2	14
2	15
2	16
2	17
3	18
3	19
3	20
3	21
3	22
3	23
3	24
3	25
4	4
4	5
4	6
4	11
4	1
\.


--
-- Data for Name: univers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.univers (id, name, description) FROM stdin;
1	Marvel	\N
2	DC	\N
3	Valiant	\N
\.


--
-- Name: author_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.author_id_seq', 1, false);


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.book_id_seq', 182, true);


--
-- Name: character_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.character_id_seq', 41, true);


--
-- Name: cover_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cover_id_seq', 199, true);


--
-- Name: event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.event_id_seq', 1, false);


--
-- Name: format_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.format_id_seq', 9, true);


--
-- Name: job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.job_id_seq', 1, false);


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.messenger_messages_id_seq', 1, false);


--
-- Name: nature_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nature_id_seq', 8, true);


--
-- Name: portrait_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.portrait_id_seq', 41, true);


--
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.team_id_seq', 4, true);


--
-- Name: univers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.univers_id_seq', 1, false);


--
-- Name: author_book author_book_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT author_book_pkey PRIMARY KEY (author_id, book_id);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_character book_character_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_character
    ADD CONSTRAINT book_character_pkey PRIMARY KEY (book_id, character_id);


--
-- Name: book_format book_format_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_format
    ADD CONSTRAINT book_format_pkey PRIMARY KEY (book_id, format_id);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: character character_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT character_pkey PRIMARY KEY (id);


--
-- Name: cover_book cover_book_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cover_book
    ADD CONSTRAINT cover_book_pkey PRIMARY KEY (cover_id, book_id);


--
-- Name: cover cover_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cover
    ADD CONSTRAINT cover_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: event event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: format format_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.format
    ADD CONSTRAINT format_pkey PRIMARY KEY (id);


--
-- Name: job_author job_author_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job_author
    ADD CONSTRAINT job_author_pkey PRIMARY KEY (job_id, author_id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- Name: nature nature_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nature
    ADD CONSTRAINT nature_pkey PRIMARY KEY (id);


--
-- Name: portrait_character portrait_character_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portrait_character
    ADD CONSTRAINT portrait_character_pkey PRIMARY KEY (portrait_id, character_id);


--
-- Name: portrait portrait_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portrait
    ADD CONSTRAINT portrait_pkey PRIMARY KEY (id);


--
-- Name: team_character team_character_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_character
    ADD CONSTRAINT team_character_pkey PRIMARY KEY (team_id, character_id);


--
-- Name: team team_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (id);


--
-- Name: univers univers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.univers
    ADD CONSTRAINT univers_pkey PRIMARY KEY (id);


--
-- Name: idx_247faed31136be75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_247faed31136be75 ON public.team_character USING btree (character_id);


--
-- Name: idx_247faed3296cd8ae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_247faed3296cd8ae ON public.team_character USING btree (team_id);


--
-- Name: idx_2f0a2bee16a2b381; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_2f0a2bee16a2b381 ON public.author_book USING btree (book_id);


--
-- Name: idx_2f0a2beef675f31b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_2f0a2beef675f31b ON public.author_book USING btree (author_id);


--
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- Name: idx_81d4a6721136be75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_81d4a6721136be75 ON public.book_character USING btree (character_id);


--
-- Name: idx_81d4a67216a2b381; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_81d4a67216a2b381 ON public.book_character USING btree (book_id);


--
-- Name: idx_937ab0343bcb2e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_937ab0343bcb2e4b ON public."character" USING btree (nature_id);


--
-- Name: idx_cbe5a3311cf61c0b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cbe5a3311cf61c0b ON public.book USING btree (univers_id);


--
-- Name: idx_cbe5a33171f7e88b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cbe5a33171f7e88b ON public.book USING btree (event_id);


--
-- Name: idx_d1fa6cef1136be75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d1fa6cef1136be75 ON public.portrait_character USING btree (character_id);


--
-- Name: idx_d1fa6cef1226ebf3; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d1fa6cef1226ebf3 ON public.portrait_character USING btree (portrait_id);


--
-- Name: idx_edabf549be04ea9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_edabf549be04ea9 ON public.job_author USING btree (job_id);


--
-- Name: idx_edabf549f675f31b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_edabf549f675f31b ON public.job_author USING btree (author_id);


--
-- Name: idx_f76d795216a2b381; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f76d795216a2b381 ON public.book_format USING btree (book_id);


--
-- Name: idx_f76d7952d629f605; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f76d7952d629f605 ON public.book_format USING btree (format_id);


--
-- Name: idx_ff39ead816a2b381; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_ff39ead816a2b381 ON public.cover_book USING btree (book_id);


--
-- Name: idx_ff39ead8922726e9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_ff39ead8922726e9 ON public.cover_book USING btree (cover_id);


--
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- Name: team_character fk_247faed31136be75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_character
    ADD CONSTRAINT fk_247faed31136be75 FOREIGN KEY (character_id) REFERENCES public."character"(id) ON DELETE CASCADE;


--
-- Name: team_character fk_247faed3296cd8ae; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_character
    ADD CONSTRAINT fk_247faed3296cd8ae FOREIGN KEY (team_id) REFERENCES public.team(id) ON DELETE CASCADE;


--
-- Name: author_book fk_2f0a2bee16a2b381; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT fk_2f0a2bee16a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: author_book fk_2f0a2beef675f31b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_book
    ADD CONSTRAINT fk_2f0a2beef675f31b FOREIGN KEY (author_id) REFERENCES public.author(id) ON DELETE CASCADE;


--
-- Name: book_character fk_81d4a6721136be75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_character
    ADD CONSTRAINT fk_81d4a6721136be75 FOREIGN KEY (character_id) REFERENCES public."character"(id) ON DELETE CASCADE;


--
-- Name: book_character fk_81d4a67216a2b381; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_character
    ADD CONSTRAINT fk_81d4a67216a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: character fk_937ab0343bcb2e4b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT fk_937ab0343bcb2e4b FOREIGN KEY (nature_id) REFERENCES public.nature(id);


--
-- Name: book fk_cbe5a3311cf61c0b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT fk_cbe5a3311cf61c0b FOREIGN KEY (univers_id) REFERENCES public.univers(id);


--
-- Name: book fk_cbe5a33171f7e88b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT fk_cbe5a33171f7e88b FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- Name: portrait_character fk_d1fa6cef1136be75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portrait_character
    ADD CONSTRAINT fk_d1fa6cef1136be75 FOREIGN KEY (character_id) REFERENCES public."character"(id) ON DELETE CASCADE;


--
-- Name: portrait_character fk_d1fa6cef1226ebf3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.portrait_character
    ADD CONSTRAINT fk_d1fa6cef1226ebf3 FOREIGN KEY (portrait_id) REFERENCES public.portrait(id) ON DELETE CASCADE;


--
-- Name: job_author fk_edabf549be04ea9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job_author
    ADD CONSTRAINT fk_edabf549be04ea9 FOREIGN KEY (job_id) REFERENCES public.job(id) ON DELETE CASCADE;


--
-- Name: job_author fk_edabf549f675f31b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.job_author
    ADD CONSTRAINT fk_edabf549f675f31b FOREIGN KEY (author_id) REFERENCES public.author(id) ON DELETE CASCADE;


--
-- Name: book_format fk_f76d795216a2b381; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_format
    ADD CONSTRAINT fk_f76d795216a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: book_format fk_f76d7952d629f605; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_format
    ADD CONSTRAINT fk_f76d7952d629f605 FOREIGN KEY (format_id) REFERENCES public.format(id) ON DELETE CASCADE;


--
-- Name: cover_book fk_ff39ead816a2b381; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cover_book
    ADD CONSTRAINT fk_ff39ead816a2b381 FOREIGN KEY (book_id) REFERENCES public.book(id) ON DELETE CASCADE;


--
-- Name: cover_book fk_ff39ead8922726e9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cover_book
    ADD CONSTRAINT fk_ff39ead8922726e9 FOREIGN KEY (cover_id) REFERENCES public.cover(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

