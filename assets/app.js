import './bootstrap.js';
import '@fortawesome/fontawesome-free/css/all.min.css';

/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.css';


// Back to top button
let back_to_top__button = document.getElementById("back-to-top__button");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 20 ||
    document.documentElement.scrollTop > 20
  ) {
    back_to_top__button.style.display = "block";
  } else {
    back_to_top__button.style.display = "none";
  }
}
// When the user clicks on the button, scroll to the top of the document
back_to_top__button.addEventListener("click", backToTop);

function backToTop() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}