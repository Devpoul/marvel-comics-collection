<?php

namespace App\Controller;

use App\Entity\Portrait;
use App\Form\PortraitType;
use App\Repository\PortraitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/portrait')]
class PortraitController extends AbstractController
{
    #[Route('/', name: 'app.portrait.index', methods: ['GET'])]
    public function index(PortraitRepository $portraitRepository): Response
    {
        return $this->render('portrait/index.html.twig', [
            'portraits' => $portraitRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app.portrait.new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $portrait = new Portrait();
        $form = $this->createForm(PortraitType::class, $portrait);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($portrait);
            $entityManager->flush();

            $this->addFlash('success', 'Le portrait a été ajouté avec succès!');

            return $this->redirectToRoute('app.portrait.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('portrait/new.html.twig', [
            'portrait' => $portrait,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.portrait.show', methods: ['GET'])]
    public function show(Portrait $portrait): Response
    {
        return $this->render('portrait/show.html.twig', [
            'portrait' => $portrait,
        ]);
    }

    #[Route('/{id}/edit', name: 'app.portrait.edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Portrait $portrait, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PortraitType::class, $portrait);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $this->addFlash('warning', 'Le portrait a été modifié avec succès!');

            return $this->redirectToRoute('app.portrait.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('portrait/edit.html.twig', [
            'portrait' => $portrait,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.portrait.delete', methods: ['POST'])]
    public function delete(Request $request, Portrait $portrait, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$portrait->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($portrait);
            $entityManager->flush();

            $this->addFlash('danger', 'Le portrait a été supprimé avec succès!');
        }

        return $this->redirectToRoute('app.portrait.index', [], Response::HTTP_SEE_OTHER);
    }
}
