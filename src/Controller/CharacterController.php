<?php

namespace App\Controller;

use App\Entity\Character;
use App\Form\CharacterType;
use App\Repository\BookRepository;
use App\Repository\CharacterRepository;
use App\Repository\TeamRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/personnage')]
class CharacterController extends AbstractController
{
    #[Route('/', name: 'app.character.index', methods: ['GET'])]
    public function index(CharacterRepository $characterRepository, TeamRepository $teamRepository): Response
    {
        $characters = $characterRepository->findAll();
        $teams      = $teamRepository->findAll();   
        return $this->render('character/index.html.twig', [
            'characters' => $characters,
            'teams'      => $teams,
        ]);
    }
    
    #[Route('/character/{id}', name: 'app.character.books', methods: ['GET'])]
    public function booksByCharacter(BookRepository $bookRepository, int $id, CharacterRepository $characterRepository): Response
    {
        return $this->render('book/character.html.twig', [
            'books'                     => $bookRepository->findBooksByCharacter($id),
            'character'                 => $characterRepository->findById($id) 
        ]);
    }

    #[Route('/ajouter', name: 'app.character.new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $character = new Character();
        $form = $this->createForm(CharacterType::class, $character);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($character);
            $entityManager->flush();

            $this->addFlash('success', 'Le personnage a été ajouté avec succès!');

            return $this->redirectToRoute('app.character.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('character/new.html.twig', [
            'character' => $character,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.character.show', methods: ['GET'])]
    public function show(Character $character): Response
    {
        return $this->render('character/show.html.twig', [
            'character' => $character,
        ]);
    }

    #[Route('/{id}/modifier', name: 'app.character.edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Character $character, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CharacterType::class, $character);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $this->addFlash('warning', 'Le personnage a été modifié avec succès!');

            return $this->redirectToRoute('app.character.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('character/edit.html.twig', [
            'character' => $character,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.character.delete', methods: ['POST'])]
    public function delete(Request $request, Character $character, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$character->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($character);
            $entityManager->flush();

            $this->addFlash('danger', 'Le personnage a été supprimé avec succès!');
        }

        return $this->redirectToRoute('app.character.index', [], Response::HTTP_SEE_OTHER);
    }
}
