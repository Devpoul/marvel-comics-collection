<?php

namespace App\Controller;

use App\Repository\UniversRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PageController extends AbstractController
{
    #[Route('/', name: 'app.page.home')]
    public function home(UniversRepository $universRepository): Response
    {
        $multivers = $universRepository->findAll();

        return $this->render('_layouts/home.html.twig', [
            'multivers' => $multivers
        ]);
    }
}
