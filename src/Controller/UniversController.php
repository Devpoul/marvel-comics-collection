<?php

namespace App\Controller;

use App\Entity\Univers;
use App\Form\UniversType;
use App\Repository\UniversRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/univers')]
class UniversController extends AbstractController
{
    #[Route('/', name: 'app.univers.index', methods: ['GET'])]
    public function index(UniversRepository $universRepository): Response
    {
        $univers = $universRepository->findAll();

        return $this->render('univers/index.html.twig', [
            'multivers'                 => $universRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app.univers.new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $univers = new Univers();
        $form = $this->createForm(UniversType::class, $univers);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($univers);
            $entityManager->flush();

            return $this->redirectToRoute('app.univers.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('univers/new.html.twig', [
            'univers' => $univers,
            'form' => $form,
        ]);
    }   

    #[Route('/{id}', name: 'app.univers.show', methods: ['GET'])]
    public function show(Univers $univers, UniversRepository $universRepository): Response
    {
        return $this->render('univers/show.html.twig', [
            'univers' => $univers,
        ]);
    }

    #[Route('/{id}/edit', name: 'app.univers.edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Univers $univers, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UniversType::class, $univers);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app.univers.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('univers/edit.html.twig', [
            'univers' => $univers,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.univers.delete', methods: ['POST'])]
    public function delete(Request $request, Univers $univers, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$univers->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($univers);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app.univers.index', [], Response::HTTP_SEE_OTHER);
    }
}
