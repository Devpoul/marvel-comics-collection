<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Repository\BookRepository;
use App\Repository\CharacterRepository;
use App\Repository\UniversRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/comic')]
class BookController extends AbstractController
{
    #[Route('/', name: 'app.book.index', methods: ['GET'])]
    public function index(BookRepository $bookRepository): Response
    {
        return $this->render('book/index.html.twig', [
            'books' => $bookRepository->findAll(),
        ]);
    }

    #[Route('/univers/{id}', name: 'app.book.univers', methods: ['GET'])]
    public function booksByUnivers(UniversRepository $universRepository, BookRepository $bookRepository, int $id, CharacterRepository $characterRepository): Response
    {
        return $this->render('book/univers.html.twig', [
            'multivers'                 => $universRepository->findAll(),
            'books'                     => $bookRepository->findByUniversSortedAlphabetically($id),
            'characters'                => $characterRepository->findByUnivers($id),
        ]);
    }

    #[Route('/ajouter', name: 'app.book.new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($book);
            $entityManager->flush();

            $this->addFlash('success', 'Le comic a été ajouté avec succès!');

            return $this->redirectToRoute('app.book.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/new.html.twig', [
            'book'                      => $book,
            'form'                      => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.book.show', methods: ['GET'])]
    public function show(Book $book): Response
    {
        return $this->render('book/show.html.twig', [
            'book'                      => $book,
        ]);
    }

    #[Route('/{id}/modifier', name: 'app.book.edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Book $book, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $this->addFlash('warning', 'Le comic a été modifié avec succès!');

            return $this->redirectToRoute('app.book.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('book/edit.html.twig', [
            'book'                      => $book,
            'form'                      => $form,
        ]);
    }

    #[Route('/{id}', name: 'app.book.delete', methods: ['POST'])]
    public function delete(Request $request, Book $book, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$book->getId(), $request->getPayload()->get('_token'))) {
            $entityManager->remove($book);
            $entityManager->flush();

            $this->addFlash('danger', 'Le comic a été supprimé avec succès!');
        }

        return $this->redirectToRoute('app.book.index', [], Response::HTTP_SEE_OTHER);
    }
}
