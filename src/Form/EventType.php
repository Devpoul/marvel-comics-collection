<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Event;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('title', TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'Titre',
                'class' => 'text-center text-secondary',
                'style' => 'opacity: 0.7'
            ]
        ])
        ->add('books', EntityType::class, [
            'class' => Book::class,
            'required' => false,
            'choice_label' => 'title',
            'multiple' => true,
            'label' => 'Comics',
            'attr' => [
                'class' => 'text-center text-secondary'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
