<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Univers;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UniversType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('books', EntityType::class, [
                'class' => Book::class,
                'required' => false,
                'choice_label' => 'title',
                'multiple' => true,
                'label' => 'Comics',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Univers::class,
        ]);
    }
}
