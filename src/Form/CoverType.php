<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Cover;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class CoverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('imageFile', FileType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Couverture',
                    'class' => 'text-center text-secondary'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/*',
                        ],
                        'mimeTypesMessage' => 'Choisir un format d\'image valide',
                    ])
                ]
            ])
            ->add('books', EntityType::class, [
                'class' => Book::class,
                'required' => false,
                'choice_label' => 'subTitle',
                'multiple' => true,
                'label' => false,
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cover::class,
        ]);
    }
}
