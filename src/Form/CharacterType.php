<?php

namespace App\Form;

use App\Entity\Character;
use App\Entity\Portrait;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('firstName', TextType::class, [
            'label' => false,
            'required' => false,
            'attr' => [
                'placeholder' => 'Prénom',
                'class' => 'text-center text-secondary',
                'style' => 'opacity: 0.7'
            ]
        ])
        ->add('lastName', TextType::class, [
            'label' => false,
            'required' => false,
            'attr' => [
                'placeholder' => 'Nom',
                'class' => 'text-center text-secondary',
                'style' => 'opacity: 0.7'
            ]
        ])
        ->add('alias', TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'Alias',
                'class' => 'text-center text-secondary',
                'style' => 'opacity: 0.7'
            ]
        ])
        ->add('side', ChoiceType::class, [
            'label' => false,
            'choices' => [
                'Héros' => true,
                'Vilain' => false,
            ],
            'attr' => [
                'class' => 'form-select'
            ]
        ])
        ->add('portraits', EntityType::class, [
            'class' => Portrait::class,
            'required' => false,
            'choice_label' => 'name',
            'multiple' => true,
            'label' => 'Portrait',
            'attr' => [
                'class' => 'text-center text-secondary'
            ]
        ])
        ->add('teams', EntityType::class, [
            'class' => Team::class,
            'required' => false,
            'choice_label' => 'name',
            'multiple' => true,
            'label' => 'Équipe',
            'attr' => [
                'class' => 'text-center text-secondary'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Character::class,
        ]);
    }
}
