<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Job;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Prénom',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nom',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('alias', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Alias',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('dateOfBirth', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Naissance',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('dateOfDeath', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Décès',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('jobs', EntityType::class, [
                'class' => Job::class,
                'choice_label' => 'id',
                'multiple' => true,
                'label' => 'Métier',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('books', EntityType::class, [
                'class' => Book::class,
                'choice_label' => 'id',
                'multiple' => true,
                'label' => 'Comics',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Author::class,
        ]);
    }
}
