<?php

namespace App\Form;

use App\Entity\Character;
use App\Entity\Team;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nom',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Description',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('characters', EntityType::class, [
                'class' => Character::class,
                'required' => false,
                'choice_label' => 'alias',
                'multiple' => true,
                'label' => 'Personnages',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
        ]);
    }
}
