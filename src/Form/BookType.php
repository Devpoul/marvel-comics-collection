<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Character;
use App\Entity\Format;
use App\Entity\Univers;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('subTitle', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Titre secondaire',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('volume', IntegerType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Volume',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('summary', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Résumé',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('pages', IntegerType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Nombre de pages',
                    'class' => 'text-center text-secondary',
                    'style' => 'opacity: 0.7'
                ]
            ])
            ->add('release', DateType::class, [
                'widget' => 'single_text',
                'label' => false,
                'attr' => [
                    'placeholder' => 'Titre',
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('formats', EntityType::class, [
                'class' => Format::class,
                'required' => false,
                'choice_label' => 'entitled',
                'multiple' => true,
                'label' => 'Formats',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('authors', EntityType::class, [
                'class' => Author::class,
                'required' => false,
                'choice_label' => 'alias',
                'multiple' => true,
                'label' => 'Auteurs',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('characters', EntityType::class, [
                'class' => Character::class,
                'required' => false,
                'choice_label' => 'alias',
                'multiple' => true,
                'label' => 'Personnages',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ])
            ->add('univers', EntityType::class, [
                'class' => Univers::class,
                'choice_label' => 'name',
                'label' => 'Univers',
                'attr' => [
                    'class' => 'text-center text-secondary'
                ]
            ]);
            // ->add('covers', EntityType::class, [
            //     'class' => Cover::class,
            //     'required' => false,
            //     'choice_label' => 'name',
            //     'multiple' => true,
            //     'label' => 'Couvertures',
            //     'attr' => [
            //         'class' => 'text-center text-secondary'
            //     ]
            // ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
