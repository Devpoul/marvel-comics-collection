<?php

namespace App\Repository;

use App\Entity\Character;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Character>
 */
class CharacterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Character::class);
    }

    /**
     * Get all characters associated with a specific universe.
     *
     * @param int $universId
     * @return Character[]
     */
    public function findByUnivers(int $universId): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.books', 'b')
            ->innerJoin('b.univers', 'u')
            ->where('u.id = :universId')
            ->setParameter('universId', $universId)
            ->distinct() // Ensures we get unique characters
            ->getQuery()
            ->getResult();
    }
}
