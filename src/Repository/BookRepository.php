<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Book>
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * Find books by universe ID and sort them alphabetically by title.
     *
     * @param int $universId
     * @return Book[]
     */
    public function findByUniversSortedAlphabetically(int $universId): array
    {
        return $this->createQueryBuilder('b')
            ->innerJoin('b.univers', 'u')
            ->where('u.id = :universId')
            ->setParameter('universId', $universId)
            ->orderBy('b.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Retrieve all books associated with a specific character.
     * 
     * @param int $characterId The ID of the character
     * @return Book[] Returns an array of Book objects
     */
    public function findBooksByCharacter(int $characterId): array
    {
        return $this->createQueryBuilder('b')
            ->innerJoin('b.characters', 'c')
            ->where('c.id = :characterId')
            ->setParameter('characterId', $characterId)
            ->orderBy('b.title', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
