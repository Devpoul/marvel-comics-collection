<?php

namespace App\EventSubscriber;

use App\Repository\UniversRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

class UniversSubscriber implements EventSubscriberInterface
{
    private UniversRepository $universRepository;
    private Environment $twig;

    public function __construct(UniversRepository $universRepository, Environment $twig)
    {
        $this->universRepository = $universRepository;
        $this->twig = $twig;
    }

    public function onControllerEvent(ControllerEvent $event): void
    {
        $universList = $this->universRepository->findAll();
        $this->twig->addGlobal('universList', $universList);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onControllerEvent',
        ];
    }
}
